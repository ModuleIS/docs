package document_management.facadeService.api;

import document_management.protocol.dto.AssignmentDTO;
import document_management.protocol.request.AssignmentRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AssignmentFacadeService {
    AssignmentDTO createAssignment(AssignmentRequest request);

    Page<AssignmentDTO> getAllAssignments(Pageable pageable);

    AssignmentDTO getAssignmentById(long id);

    void deleteAssignment(long id);

    AssignmentDTO updateAssignment(AssignmentRequest request);
}
