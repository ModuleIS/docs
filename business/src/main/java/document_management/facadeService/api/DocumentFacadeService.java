package document_management.facadeService.api;

import document_management.protocol.dto.DocumentCardDTO;
import document_management.protocol.request.CreateDocumentCardRequest;

import java.util.List;

public interface DocumentFacadeService {

    DocumentCardDTO createDocument(CreateDocumentCardRequest request);

    List<DocumentCardDTO> getDocuments(int page, int limit);

    void deleteDocument(long id);

    DocumentCardDTO getDocumentById(long id);
}

