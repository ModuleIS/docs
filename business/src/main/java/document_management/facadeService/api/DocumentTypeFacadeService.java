package document_management.facadeService.api;

import document_management.protocol.dto.DocumentFieldDTO;
import document_management.protocol.dto.DocumentTypeDTO;
import document_management.protocol.dto.DocumentTypeFullDTO;
import document_management.protocol.request.DocumentTypeRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface DocumentTypeFacadeService {

    List<String> getAllDocumentActions();

    List<DocumentFieldDTO> getAllDocumentFields();

    DocumentFieldDTO createDocumentField(DocumentFieldDTO request);

    DocumentFieldDTO changeDocumentField(DocumentFieldDTO request);

    void deleteDocumentField(long id);

    List<DocumentTypeDTO> getAllDocumentTypes();

    void deleteDocumentType(long id);

    DocumentTypeFullDTO changeDocumentType(DocumentTypeRequest request);

    DocumentTypeFullDTO createDocumentType(DocumentTypeRequest request);

    Page<DocumentTypeDTO> getAllDocumentTypesPage(String filterByName, Pageable pageable);

    DocumentTypeFullDTO getAllDocumentTypeById(long id);

    DocumentTypeFullDTO addTemplateForType(long id, MultipartFile file);

    void deleteTemplateForType(long id, long templateId);

    List<String> getAllDocumentFieldTypes();
}
