package document_management.facadeService.api;

import document_management.protocol.dto.FolderFullDTO;

import java.util.List;

public interface FolderSystemFacadeService {
    List<FolderFullDTO> getAllFolders();
}
