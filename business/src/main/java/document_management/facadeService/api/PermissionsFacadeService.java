package document_management.facadeService.api;

import document_management.protocol.dto.PermissionGroupDTO;
import document_management.protocol.dto.PermissionGroupFullDTO;
import document_management.protocol.request.PermissionGroupRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PermissionsFacadeService {
    PermissionGroupDTO createPermissionGroup(PermissionGroupRequest request);

    PermissionGroupDTO changePermissionGroup(PermissionGroupRequest request);

    void deletePermissionGroup(long id);

    Page<PermissionGroupDTO> getAllPermissionGroups(String filterByName, Pageable pageable);

    PermissionGroupFullDTO getPermissionGroupById(long id);

    List<PermissionGroupDTO> getAllPermissionGroups();
}
