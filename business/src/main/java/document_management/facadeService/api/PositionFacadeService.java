package document_management.facadeService.api;

import document_management.protocol.dto.PositionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PositionFacadeService {

    Page<PositionDTO> getAllUserPositions(String filterByName, Pageable pageable);

    List<PositionDTO> getAllPositions();

    PositionDTO createPositions(PositionDTO request);

    PositionDTO changePosition(PositionDTO request);

    void deletePosition(long id);
}
