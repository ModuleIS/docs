package document_management.facadeService.api;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageFileFacadeService {

    String uploadFile(MultipartFile file);

    Resource loadFile(String filename);
}
