package document_management.facadeService.api;

import document_management.entity.user.User;
import document_management.protocol.dto.PasswordData;
import document_management.protocol.dto.UserDto;
import document_management.protocol.request.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface UserFacadeService {
    UserDto findBySecondName(String secondName);

    Boolean existByPositionId(Long positionId);

    UserDto findUserById(Long id);

    UserDto createUser(CreateUserRequest request);

    UserDto findByFullName(String firstName, String secondName, String surname);

    UserDto changeRole(ChangeRoleRequest request);

    UserDto updateUser(UpdateUserRequest request);

    UserDto changeUserStatus(ChangeStatusRequest request);

    void sendPassword(ReminderPasswordRequest request);

    Page<UserDto> getUserByFilter(Specification<User> userSpecification, Pageable pageable);

    PasswordData generatePassword();

    Boolean resetPassword(ResetPasswordRequest request);

    void activation(UserDto userDto);

    void setOffline(Long userId);

    void setOnline(Long userId);
}
