package document_management.facadeService.impl;

import document_management.entity.assignment.Assignment;
import document_management.entity.assignment.AssignmentStatus;
import document_management.facadeService.api.AssignmentFacadeService;
import document_management.mapper.impl.AssignmentMapper;
import document_management.protocol.dto.AssignmentDTO;
import document_management.protocol.request.AssignmentRequest;
import document_management.service.api.AssignmentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class AssignmentFacadeServiceImpl implements AssignmentFacadeService {

    private final AssignmentService assignmentService;
    private final AssignmentMapper mapper;

    @Override
    public AssignmentDTO createAssignment(AssignmentRequest request) {
        Assignment newAssignment = mapper.requestToEntity(request);
        newAssignment.setStatus(AssignmentStatus.OPEN);

        assignmentService.createAssignment(newAssignment);
        return mapper.entityToDto(newAssignment);
    }

    @Override
    public Page<AssignmentDTO> getAllAssignments(Pageable pageable) {
        return assignmentService.getAllAssignments(pageable)
                .map(mapper::entityToDto);
    }

    @Override
    public AssignmentDTO getAssignmentById(long id) {
        return mapper.entityToDto(assignmentService.getAssignmentById(id));
    }

    @Override
    public void deleteAssignment(long id) {
        assignmentService.deleteById(id);
    }

    @Override
    public AssignmentDTO updateAssignment(AssignmentRequest request) {
        Assignment fromDd = assignmentService.getAssignmentById(request.getId());
        Assignment toUpdate = mapper.requestToEntity(request);
        updateAssignmentFields(fromDd, toUpdate);

        return mapper.entityToDto(assignmentService.createAssignment(toUpdate));
    }

    private void updateAssignmentFields(Assignment fromDd, Assignment toUpdate) {
        toUpdate.setId(fromDd.getId());
    }
}
