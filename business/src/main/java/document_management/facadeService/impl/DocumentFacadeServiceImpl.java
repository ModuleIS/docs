package document_management.facadeService.impl;

import document_management.entity.document.DocumentCard;
import document_management.entity.document.DocumentStatus;
import document_management.facadeService.api.DocumentFacadeService;
import document_management.mapper.impl.DocumentCardMapper;
import document_management.protocol.dto.DocumentCardDTO;
import document_management.protocol.request.CreateDocumentCardRequest;
import document_management.service.api.DocumentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
public class DocumentFacadeServiceImpl implements DocumentFacadeService {

    private final DocumentService documentService;
    private final DocumentCardMapper modelMapper;

    @Override
    public DocumentCardDTO createDocument(CreateDocumentCardRequest request) {
        DocumentCard newDocument = modelMapper.requestToEntity(request);
        newDocument.setDocumentStatus(DocumentStatus.NEW);

        DocumentCard documentCard = documentService.createDocument(newDocument);
        return modelMapper.entityToDto(documentCard);
    }

    @Override
    public List<DocumentCardDTO> getDocuments(int page, int limit) {
        Page<DocumentCard> documentCardsPage = documentService.getDocuments(page, limit);
        return documentCardsPage.get()
                .map(modelMapper::entityToDto)
                .collect(toList());
    }

    @Override
    public void deleteDocument(long id) {
        documentService.archiveDocument(id);
    }

    @Override
    public DocumentCardDTO getDocumentById(long id) {
        DocumentCard documentCard = documentService.getDocumentById(id);
        return modelMapper.entityToDto(documentCard);
    }

}
