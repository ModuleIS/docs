package document_management.facadeService.impl;

import document_management.entity.document.DocumentField;
import document_management.entity.document.DocumentType;
import document_management.entity.document.FieldType;
import document_management.entity.document.Template;
import document_management.exeption.FileUploadArgumentException;
import document_management.exeption.ResourceAlreadyExistsException;
import document_management.exeption.ResourceDeleteException;
import document_management.exeption.ResourceNotFoundException;
import document_management.facadeService.api.DocumentTypeFacadeService;
import document_management.mapper.impl.DocumentFieldMapper;
import document_management.mapper.impl.DocumentTypeMapper;
import document_management.protocol.dto.DocumentFieldDTO;
import document_management.protocol.dto.DocumentTypeDTO;
import document_management.protocol.dto.DocumentTypeFullDTO;
import document_management.protocol.request.DocumentTypeRequest;
import document_management.service.api.DocumentFieldService;
import document_management.service.api.DocumentTypeService;
import document_management.service.api.StorageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Slf4j
@Transactional
@Service
@AllArgsConstructor
public class DocumentTypeFacadeServiceImpl implements DocumentTypeFacadeService {

    private final DocumentTypeService documentTypeService;
    private final DocumentFieldService documentFieldService;
    private final DocumentFieldMapper fieldMapper;
    private final DocumentTypeMapper typeMapper;
    private final StorageService storageService;


    @Override
    @Transactional(readOnly = true)
    public List<String> getAllDocumentActions() {
        return documentTypeService.getAllDocumentActions();
    }

    @Override
    @Transactional(readOnly = true)
    public List<DocumentFieldDTO> getAllDocumentFields() {
        return documentFieldService.getAllFields().stream()
                .map(fieldMapper::toDto)
                .collect(toList());
    }

    @Override
    public DocumentFieldDTO createDocumentField(DocumentFieldDTO request) {
        if(existFieldByName(request.getName())) {
            throw  new ResourceAlreadyExistsException("DocumentField with name [" + request.getName() + "] already exists!");
        }
        DocumentField newField = fieldMapper.toEntity(request);
        if(Objects.isNull(newField.getType())){
            newField.setType(FieldType.TEXT);
        }
        newField = documentFieldService.saveField(newField);
        return fieldMapper.toDto(newField);
    }

    @Override
    public DocumentFieldDTO changeDocumentField(DocumentFieldDTO request) {
        if(existFieldByName(request.getName())) {
            throw  new ResourceAlreadyExistsException("DocumentField with name [" + request.getName() + "] already exists!");
        }
        DocumentField fromDB = documentFieldService.getById(request.getId());
        fromDB.setName(request.getName());
        fromDB.setType(FieldType.valueOf(request.getType()));
        documentFieldService.saveField(fromDB);
        return fieldMapper.toDto(fromDB);
    }

    @Override
    public void deleteDocumentField(long id) {
        documentFieldService.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DocumentTypeDTO> getAllDocumentTypes() {
        return documentTypeService.getAllDocumentTypes().stream()
                .map(typeMapper::entityToDto)
                .collect(toList());
    }

    @Override
    public void deleteDocumentType(long id) {
        DocumentType documentType = documentTypeService.getDocumentTypeById(id);

        List<Template> templatesToDelete = new ArrayList<>();
        for(Template template : documentType.getTemplates()){
            if(deleteTemplateFromFileSystem(template)){
                templatesToDelete.add(template);
            }
        }
        documentType.getTemplates().removeAll(templatesToDelete);
        if(documentType.getTemplates().isEmpty()){
            documentTypeService.deleteTypeById(id);
        } else {
            throw new ResourceDeleteException("Some templates couldn't be deleted");
        }
    }

    @Override
    public DocumentTypeFullDTO changeDocumentType(DocumentTypeRequest request) {
        if(existTypeByName(request.getName())) {
            throw  new ResourceAlreadyExistsException("DocumentType with name [" + request.getName() + "] already exists!");
        }
        DocumentType fromDB = documentTypeService.getDocumentTypeById(request.getId());
        DocumentType updateRequest = typeMapper.requestToEntity(request);
        updateDocumentType(fromDB, updateRequest);
        fromDB = documentTypeService.saveType(fromDB);
        return typeMapper.toComplexDto(fromDB);
    }

    private void updateDocumentType(DocumentType fromDB, DocumentType updateRequest) {
        fromDB.setFields(updateRequest.getFields());
        fromDB.setActions(updateRequest.getActions());
        fromDB.setName(updateRequest.getName());
    }

    @Override
    public DocumentTypeFullDTO createDocumentType(DocumentTypeRequest request) {
        if(existTypeByName(request.getName())) {
            throw  new ResourceAlreadyExistsException("DocumentType with name [" + request.getName() + "] already exists!");
        }
        DocumentType newType = typeMapper.requestToEntity(request);
        newType = documentTypeService.saveType(newType);
        return typeMapper.toComplexDto(newType);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DocumentTypeDTO> getAllDocumentTypesPage(String filterByName, Pageable pageable) {
        return documentTypeService.getAllDocumentTypes(filterByName, pageable)
                .map( typeMapper::entityToDto);
    }

    @Override
    public DocumentTypeFullDTO getAllDocumentTypeById(long id) {
        return typeMapper.toComplexDto(documentTypeService.getDocumentTypeById(id));
    }

    @Override
    public DocumentTypeFullDTO addTemplateForType(long id, MultipartFile file) {
        if(Objects.isNull(file) || file.isEmpty()){
            throw new FileUploadArgumentException("Empty file to upload");
        }
        DocumentType type = documentTypeService.getDocumentTypeById(id);
        String fileKey = storageService.saveFile(file);
        if(fileKey == null) {
            throw new FileUploadArgumentException("Empty file to upload");
        }
        Template template = createTemplate(file.getOriginalFilename(), fileKey);
        type.getTemplates().add(template);

        documentTypeService.saveType(type);

        return typeMapper.toComplexDto(type);
    }

    @Override
    public void deleteTemplateForType(long id, long templateId) {
        DocumentType type = documentTypeService.getDocumentTypeById(id);
        Template template = type.getTemplates().stream()
                .filter(t -> t.getId().equals(templateId))
                .findAny()
                .orElseThrow(() -> new ResourceNotFoundException(Template.class.getSimpleName() +
                        " is not exist with id = " + templateId));

        if(deleteTemplateFromFileSystem(template)){
            type.getTemplates().remove(template);
        }
        documentTypeService.saveType(type);

    }

    @Override
    public List<String> getAllDocumentFieldTypes() {
        return documentFieldService.getAllFieldTypes();
    }

    private boolean deleteTemplateFromFileSystem(Template template) {
        if(storageService.deleteFile(template.getKey())){
            log.info("File has been deleted successfully" + template.getKey());
            return  true;

        } else log.warn("File deleting error" + template.getKey());
        return false;
    }

    private Template createTemplate(String originalFilename, String fileKey) {
        String extension= FilenameUtils.getExtension(originalFilename);
        String name = FilenameUtils.getBaseName(originalFilename);
        Template template = new Template();
        template.setExtension(extension);
        template.setName(name);
        template.setKey(fileKey);
        return template;
    }

    private boolean existFieldByName(String name) {
        return  documentFieldService.existsByName(name);
    }
    private boolean existTypeByName(String name) {
        return  documentTypeService.existsByName(name);
    }

}
