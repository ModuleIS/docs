package document_management.facadeService.impl;

import document_management.entity.folder.Folder;
import document_management.facadeService.api.FolderSystemFacadeService;
import document_management.protocol.dto.FolderFullDTO;
import document_management.service.api.FolderSystemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class FolderSystemFacadeServiceImpl implements FolderSystemFacadeService {

    private final FolderSystemService folderSystemService;
    private final ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public List<FolderFullDTO> getAllFolders() {
        List<Folder> rootFolders = folderSystemService.getAllFolders();
        log.info("Get root folders, size = [{}]", rootFolders.size());

        return rootFolders.stream()
                .map(f -> modelMapper.map(f, FolderFullDTO.class))
                .collect(Collectors.toList());
    }
}
