package document_management.facadeService.impl;

import document_management.entity.permission.PermissionGroup;
import document_management.facadeService.api.PermissionsFacadeService;
import document_management.mapper.impl.PermissionGroupMapper;
import document_management.protocol.dto.PermissionGroupDTO;
import document_management.protocol.dto.PermissionGroupFullDTO;
import document_management.protocol.request.PermissionGroupRequest;
import document_management.service.api.PermissionsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class PermissionsFacadeServiceImpl implements PermissionsFacadeService {

    private final PermissionsService permissionsService;
    private final PermissionGroupMapper modelMapper;

    @Override
    public PermissionGroupDTO createPermissionGroup(PermissionGroupRequest request) {
        PermissionGroup groupToSave = modelMapper.requestToEntity(request);
        PermissionGroup permissionGroup = permissionsService.createPermissionGroup(groupToSave);
        return modelMapper.entityToDto(permissionGroup);
    }



    @Override
    public PermissionGroupDTO changePermissionGroup(PermissionGroupRequest request) {
        PermissionGroup groupToSave = permissionsService.findById(request.getId());
        PermissionGroup changeUpdate = modelMapper.requestToEntity(request);
        updatePermissionGroup(groupToSave, changeUpdate);

        PermissionGroup permissionGroup = permissionsService.createPermissionGroup(groupToSave);

        return modelMapper.entityToDto(permissionGroup);
    }

    private void updatePermissionGroup(PermissionGroup groupToSave, PermissionGroup changeUpdate) {
        groupToSave.setName(changeUpdate.getName());
        groupToSave.setDescription(changeUpdate.getDescription());
        groupToSave.setAccess(changeUpdate.getAccess());
        groupToSave.setMembers(changeUpdate.getMembers());
        groupToSave.setDocuments(changeUpdate.getDocuments());
        groupToSave.setFolders(changeUpdate.getFolders());
        groupToSave.setPositions(changeUpdate.getPositions());
    }

    @Override
    public void deletePermissionGroup(long id) {
        permissionsService.deletePermissionGroup(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PermissionGroupDTO> getAllPermissionGroups(String filterByName, Pageable pageable) {

        Page<PermissionGroup> groups = permissionsService.getAll(filterByName, pageable);
        log.info("Get permission groups, size = [{}]", groups.getNumberOfElements());

        return groups.map(modelMapper::entityToDto);
    }

    @Override
    @Transactional(readOnly = true)
    public PermissionGroupFullDTO getPermissionGroupById(long id) {
        PermissionGroup permissionGroup = permissionsService.findById(id);
        return  modelMapper.toComplexDto(permissionGroup);
    }

    @Override
    public List<PermissionGroupDTO> getAllPermissionGroups() {
        List<PermissionGroup> permissionGroups = permissionsService.getAll();
        return permissionGroups.stream()
                .map(modelMapper::entityToDto)
                .collect(Collectors.toList());
    }

}
