package document_management.facadeService.impl;

import document_management.entity.user.Position;
import document_management.exeption.ExistUserPositionException;
import document_management.exeption.ResourceAlreadyExistsException;
import document_management.facadeService.api.PositionFacadeService;
import document_management.facadeService.api.UserFacadeService;
import document_management.protocol.dto.PositionDTO;
import document_management.service.api.PositionService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class PositionFacadeServiceImpl implements PositionFacadeService {
    private final UserFacadeService userFacadeService;
    private final PositionService positionService;
    private final ModelMapper modelMapper;

    @Override
    public Page<PositionDTO> getAllUserPositions(String filterByName, Pageable pageable) {
        return positionService.getAllPositions(filterByName, pageable)
                .map(p -> modelMapper.map(p, PositionDTO.class));
    }

    @Override
    public List<PositionDTO> getAllPositions() {
        return positionService.getAll().stream()
                .map(p -> modelMapper.map(p, PositionDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public PositionDTO createPositions(PositionDTO request) {
        if (existByName(request.getName())) {
            throw new ResourceAlreadyExistsException("Position with name [" + request.getName() + "] already exists!");
        }
        Position newPosition = modelMapper.map(request, Position.class);
        newPosition = positionService.savePosition(newPosition);
        return modelMapper.map(newPosition, PositionDTO.class);
    }

    @Override
    public PositionDTO changePosition(PositionDTO request) {
        if (existByName(request.getName())) {
            throw new ResourceAlreadyExistsException("Position with name [" + request.getName() + "] already exists!");
        }
        Position positionFromDB = positionService.getById(request.getId());
        positionFromDB.setName(request.getName());
        positionService.savePosition(positionFromDB);
        return modelMapper.map(positionFromDB, PositionDTO.class);
    }

    private boolean existByName(String name) {
        return positionService.existByName(name);
    }

    @Override
    public void deletePosition(long id) {
        if (!userFacadeService.existByPositionId(id)) {
            positionService.deleteById(id);
        } else throw new ExistUserPositionException();
    }

}
