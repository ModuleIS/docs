package document_management.facadeService.impl;

import document_management.facadeService.api.StorageFileFacadeService;
import document_management.service.api.StorageService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@AllArgsConstructor
public class StorageFileFacadeServiceImpl implements StorageFileFacadeService {

    private  final StorageService storageService;

    @Override
    public String uploadFile(MultipartFile file) {
        return storageService.saveFile(file);
    }

    @Override
    public Resource loadFile(String filename){
        return storageService.load(filename);
    }
}
