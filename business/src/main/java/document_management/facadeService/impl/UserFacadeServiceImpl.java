package document_management.facadeService.impl;

import document_management.entity.user.Position;
import document_management.entity.user.User;
import document_management.entity.user.UserRole;
import document_management.entity.user.UserStatus;
import document_management.exeption.EmailNotExistException;
import document_management.exeption.UserExistException;
import document_management.exeption.UserRoleException;
import document_management.exeption.UserStatusExistException;
import document_management.facadeService.api.UserFacadeService;
import document_management.protocol.dto.PasswordData;
import document_management.protocol.dto.UserDto;
import document_management.protocol.enums.EmailType;
import document_management.protocol.request.*;
import document_management.service.api.EmailService;
import document_management.service.api.PasswordGenerator;
import document_management.service.api.PositionService;
import document_management.service.api.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class UserFacadeServiceImpl implements UserFacadeService {
    private final UserService userService;
    private final EmailService emailService;
    private final ModelMapper modelMapper;
    private final PasswordGenerator passwordGenerator;
    private final PositionService positionService;

    @Override
    public UserDto findBySecondName(String secondName) {
        return modelMapper.map(userService.findBySecondName(secondName), UserDto.class);
    }

    @Override
    public Boolean existByPositionId(Long positionId) {
        return userService.existByPositionId(positionId);
    }

    @Override
    public UserDto findUserById(Long id) {
        return  modelMapper.map(userService.findById(id), UserDto.class);
    }

    @Override
    public UserDto createUser(CreateUserRequest request) {
        if (!checkExistUser(request)) {
            User newUser = modelMapper.map(request, User.class);
            createUser(newUser, request);
            emailService.sendEmailByType(EmailType.NEW_USER, newUser.getId(), newUser.getFirstName(), newUser.getEmail(), request.getPassword());
            return modelMapper.map(newUser, UserDto.class);
        } else throw new UserExistException("User exist with email or name and second name.");
    }

    @Override
    public UserDto findByFullName(String firstName, String secondName, String surname) {
        return modelMapper.map(userService.findByFullName(firstName, secondName, surname), UserDto.class);
    }

    @Override
    public UserDto changeRole(ChangeRoleRequest request) {
        User user = userService.findById(request.getId());
        UserRole newRole = UserRole.valueOf(request.getRole());
        if (!user.getRole().equals(newRole)) {
            user.setRole(newRole);
            return modelMapper.map(userService.save(user), UserDto.class);
        } else throw new UserRoleException();
    }

    @Override
    @Transactional
    public UserDto updateUser(UpdateUserRequest request) {
        User user = userService.findById(request.getId());
        if (!request.getFirstName().equals(user.getFirstName())) {
            user.setFirstName(request.getFirstName());
        }
        if (!request.getSecondName().equals(user.getSecondName())) {
            user.setSecondName(request.getSecondName());
        }
        if (!request.getPatronymic().equals(user.getPatronymic())) {
            user.setPatronymic(request.getPatronymic());
        }
        if (!request.getEmail().equals(user.getEmail())) {
            user.setEmail(request.getEmail());
        }
        if (!request.getPhone().equals(user.getPhone())) {
            user.setPhone(request.getPhone());
        }
        if (!request.getPositionId().equals(user.getPosition().getId())) {
            Position newPosition = positionService.getById(request.getPositionId());
            user.setPosition(newPosition);
        }
        if (!UserRole.valueOf(request.getRole()).equals(user.getRole())) {
            user.setRole(UserRole.valueOf(request.getRole()));
        }
        User updatedUser = userService.save(user);
        return modelMapper.map(updatedUser, UserDto.class);
    }

    @Override
    public UserDto changeUserStatus(ChangeStatusRequest request) {
        User user = userService.findById(request.getId());
        if (!UserStatus.valueOf(request.getStatus()).equals(user.getStatus())) {
            user.setStatus(UserStatus.valueOf(request.getStatus()));
            User updatedUser = userService.save(user);
            return modelMapper.map(updatedUser, UserDto.class);
        } else throw new UserStatusExistException();
    }

    @Override
    public void sendPassword(ReminderPasswordRequest request) {
        User user = userService.findById(request.getId());
        PasswordData data = generatePassword();
        user.setPassword(data.getEncodePassword());
        userService.save(user);
        emailService.sendEmailByType(EmailType.FORGOT_PASSWORD, user.getId(), user.getFirstName(), user.getEmail(), data.getPassword());
    }


    @Override
    public Page<UserDto> getUserByFilter(Specification<User> userSpecification, Pageable pageable) {
        return userService.getAllUserByFilter(userSpecification, pageable)
                .map(user -> modelMapper.map(user, UserDto.class));
    }

    @Override
    public PasswordData generatePassword() {
        return passwordGenerator.generatePassword();
    }

    @Override
    public Boolean resetPassword(ResetPasswordRequest request) {
        if (userService.existUserByEmail(request.getEmail())) {
            PasswordData data = generatePassword();
            emailService.sendEmailByType(EmailType.FORGOT_PASSWORD, null, null, request.getEmail(), data.getPassword());
            return true;
        } else throw new EmailNotExistException();
    }

    @Override
    public void activation(UserDto userDto) {
        User activateUser = userService.findById(userDto.getId());
        if(UserStatus.valueOf(userDto.getStatus()).equals(UserStatus.INVITATION_SENT) && !userDto.isActivate()){
            activateUser.setStatus(UserStatus.ACTIVE);
            activateUser.setActivate(true);
            userService.save(activateUser);
        } else activateUser.setOnline(true);
    }

    public void setOffline(Long userId){
        User user = userService.findById(userId);
        if(user.isOnline()) user.setOnline(false);
        userService.save(user);
    }

    @Override
    public void setOnline(Long userId) {
        User user = userService.findById(userId);
        if(!user.isOnline()) user.setOnline(true);
        userService.save(user);
    }

    private Boolean checkExistUser(CreateUserRequest request) {
        Boolean checkEmail = userService.existUserByEmail(request.getEmail());
        Boolean checkNameAndLastName = userService.existsByFirstNameAndSecondName(request.getFirstName(), request.getSecondName());
        return checkEmail && checkNameAndLastName;
    }

    private void createUser(User newUser, CreateUserRequest request) {
        newUser.setId(null);
        newUser.setRole(UserRole.valueOf(request.getRole().toUpperCase()));
        newUser.setPassword(passwordGenerator.encodePassword(request.getPassword()));
        Position position = positionService.getById(request.getPositionId());
        newUser.setPosition(position);
        newUser.setStatus(UserStatus.ACTIVE);
        newUser.setOnline(false);
        newUser.setActivate(false);
        userService.createUser(newUser);
    }
}
