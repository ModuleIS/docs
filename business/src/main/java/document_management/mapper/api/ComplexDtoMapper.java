package document_management.mapper.api;

public interface ComplexDtoMapper<E, D> {

    D toComplexDto(E entity);

}
