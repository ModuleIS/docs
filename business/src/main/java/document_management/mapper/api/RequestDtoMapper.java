package document_management.mapper.api;

public interface RequestDtoMapper<E, D, R> {

    E requestToEntity(R request);

    D entityToDto(E entity);
}
