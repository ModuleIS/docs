package document_management.mapper.api;

public interface SimpleDtoMapper <E, D> {

    E toEntity(D dto);

    D toDto(E entity);
}