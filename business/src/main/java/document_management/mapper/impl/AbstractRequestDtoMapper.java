package document_management.mapper.impl;

import document_management.mapper.api.RequestDtoMapper;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;

import java.util.Objects;

public abstract class AbstractRequestDtoMapper<E,D,R> implements RequestDtoMapper<E,D,R> {

    protected ModelMapper mapper;
    private Class<E> entityClass;
    private Class<D> dtoClass;
    private Class<R> requestClass;

    public AbstractRequestDtoMapper(ModelMapper mapper, Class<E> entityClass, Class<D> dtoClass, Class<R> requestClass) {
        this.mapper = mapper;
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
        this.requestClass = requestClass;
        initMapper();
    }

    @Override
    public E requestToEntity(R request) {
        return Objects.isNull(request) ? null : mapper.map(request, entityClass);
    }

    @Override
    public D entityToDto(E entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, dtoClass);
    }

    protected void initMapper() {
        mapper.createTypeMap(entityClass, dtoClass)
                .setPostConverter(fromEntityToDtoConverter());
        mapper.createTypeMap(requestClass, entityClass)
                .setPostConverter(fromRequestToEntityConverter());

    }

    protected Converter<E, D> fromEntityToDtoConverter() {
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            mapEntityToDtoSpecificFields(source, destination);
            return context.getDestination();
        };
    }


    protected Converter<R, E> fromRequestToEntityConverter() {
        return context -> {
            R source = context.getSource();
            E destination = context.getDestination();
            mapRequestToEntitySpecificFields(source, destination);
            return context.getDestination();
        };
    }

    protected  void mapRequestToEntitySpecificFields(R source, E destination) {}
    protected  void mapEntityToDtoSpecificFields(E source, D destination){}

}
