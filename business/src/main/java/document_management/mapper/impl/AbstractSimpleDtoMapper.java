package document_management.mapper.impl;

import document_management.mapper.api.SimpleDtoMapper;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public abstract class AbstractSimpleDtoMapper<E,D> implements SimpleDtoMapper<E,D> {

    protected ModelMapper mapper;
    private Class<E> entityClass;
    private Class<D> dtoClass;

    public AbstractSimpleDtoMapper(ModelMapper mapper, Class<E> entityClass, Class<D> dtoClass) {
        this.mapper = mapper;
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
        initMapper();
    }

    @Override
    public E toEntity(D dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, entityClass);
    }

    @Override
    public D toDto(E entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, dtoClass);
    }

    protected void initMapper() {
        mapper.createTypeMap(entityClass, dtoClass)
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(dtoClass, entityClass)
                .setPostConverter(toEntityConverter());
    }

    private Converter<E, D> toDtoConverter() {
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            mapEntitySpecificFields(source, destination);
            return context.getDestination();
        };
    }

    private Converter<D, E> toEntityConverter() {
        return context -> {
            D source = context.getSource();
            E destination = context.getDestination();
            mapDtoSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    protected void mapEntitySpecificFields(E source, D destination) {
    }

    protected void mapDtoSpecificFields(D source, E destination) {
    }
}
