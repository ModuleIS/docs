package document_management.mapper.impl;

import document_management.entity.assignment.Assignment;
import document_management.entity.document.DocumentCard;
import document_management.entity.user.User;
import document_management.exeption.ResourceNotFoundException;
import document_management.protocol.dto.AssignmentDTO;
import document_management.protocol.request.AssignmentRequest;
import document_management.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class AssignmentMapper extends AbstractRequestDtoMapper<Assignment, AssignmentDTO, AssignmentRequest>{

    private final UserRepository userRepository;

    @Autowired
    public AssignmentMapper(ModelMapper mapper, UserRepository userRepository) {
        super(mapper, Assignment.class, AssignmentDTO.class, AssignmentRequest.class);
        this.userRepository = userRepository;
    }
    @Override
    protected  void mapRequestToEntitySpecificFields(AssignmentRequest source, Assignment destination) {
        destination.setAuthor(getUserById(source.getAuthorId()));
        destination.setDocument(Objects.isNull(source.getDocumentId())? null : getDocumentById(source.getDocumentId()));
        destination.setPerformer(Objects.isNull(source.getPerformerId())? null : getUserById(source.getPerformerId()));
    }

    private User getUserById(long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName() +
                        " is not exist with id = " + id));
    }

    private DocumentCard getDocumentById(long id) {
        return new DocumentCard(id);
    }

}
