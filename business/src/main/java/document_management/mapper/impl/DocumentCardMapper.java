package document_management.mapper.impl;

import document_management.entity.document.*;
import document_management.entity.user.User;
import document_management.exeption.ResourceNotFoundException;
import document_management.protocol.dto.DocumentCardDTO;
import document_management.protocol.dto.DocumentFieldValueDTO;
import document_management.protocol.dto.UserDto;
import document_management.protocol.request.CreateDocumentCardRequest;
import document_management.repository.DocumentFieldRepository;
import document_management.repository.DocumentTypeRepository;
import document_management.repository.PermissionsRepository;
import document_management.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class DocumentCardMapper extends AbstractRequestDtoMapper<DocumentCard, DocumentCardDTO, CreateDocumentCardRequest> {

    @Autowired
    private DocumentFieldRepository fieldRepository;
    @Autowired
    private DocumentTypeRepository typeRepository;
    @Autowired
    private PermissionsRepository permissionsRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    public DocumentCardMapper(ModelMapper mapper) {
        super(mapper, DocumentCard.class, DocumentCardDTO.class, CreateDocumentCardRequest.class);
    }


    @Override
    protected void mapRequestToEntitySpecificFields(CreateDocumentCardRequest source, DocumentCard destination) {
        destination.setType(Objects.isNull(source.getType())? null :
                getDocumentType(source.getType()));

        destination.setPermissionGroups(Objects.isNull(source.getPermissionGroups()) ? new ArrayList<>() :
                permissionsRepository.findAllById(source.getPermissionGroups()));

        destination.setDocumentFields(Objects.isNull(source.getDocumentFields()) ? null :
                source.getDocumentFields().entrySet().stream()
                        .collect(Collectors.toMap(getDocumentField(),
                                Map.Entry::getValue)));
    }

    @Override
    protected void mapEntityToDtoSpecificFields(DocumentCard source, DocumentCardDTO destination){

        destination.setDocumentFields(Objects.isNull(source.getDocumentFields()) ? null :
                source.getDocumentFields().entrySet().stream()
                        .map(createFieldWithValue())
                        .collect(Collectors.toList()));
    }

    @NonNull
    private Function<Map.Entry<DocumentField, String>, DocumentFieldValueDTO> createFieldWithValue() {
        return entry -> {
            Object value = entry.getValue();
            if(FieldType.USER.equals(entry.getKey().getType())){
                value = userRepository.findById(Long.parseLong(entry.getValue()))
                        .map(u -> mapper.map(u, UserDto.class))
                        .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName() +
                        " is not exist with id = " + entry.getValue()));
            }
            return new DocumentFieldValueDTO(entry.getKey().getName(), entry.getKey().getType().name(), value);
        };
    }


    private DocumentType getDocumentType(Long id) {
        return typeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(DocumentType.class.getSimpleName() +
                " is not exist with id = " + id));
    }

    private Function<Map.Entry<Long, String>, DocumentField> getDocumentField() {
        return entry -> fieldRepository.findById(entry.getKey())
                .orElseThrow(() -> new ResourceNotFoundException(DocumentField.class.getSimpleName() +
                        " is not exist with id = " + entry.getKey()));
    }

}
