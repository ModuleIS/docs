package document_management.mapper.impl;

import document_management.entity.document.DocumentField;
import document_management.protocol.dto.DocumentFieldDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DocumentFieldMapper extends AbstractSimpleDtoMapper<DocumentField, DocumentFieldDTO> {

    @Autowired
    public DocumentFieldMapper(ModelMapper mapper) {
        super(mapper, DocumentField.class, DocumentFieldDTO.class);
    }

}
