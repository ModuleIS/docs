package document_management.mapper.impl;

import document_management.entity.document.DocumentType;
import document_management.mapper.api.ComplexDtoMapper;
import document_management.protocol.dto.DocumentTypeDTO;
import document_management.protocol.dto.DocumentTypeFullDTO;
import document_management.protocol.request.DocumentTypeRequest;
import document_management.repository.DocumentFieldRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Objects;

@Component
public class DocumentTypeMapper extends AbstractRequestDtoMapper<DocumentType, DocumentTypeDTO, DocumentTypeRequest>
    implements ComplexDtoMapper<DocumentType, DocumentTypeFullDTO> {

    @Autowired
    private DocumentFieldRepository fieldRepository;

    @Autowired
    public DocumentTypeMapper(ModelMapper mapper) {
        super(mapper, DocumentType.class, DocumentTypeDTO.class, DocumentTypeRequest.class);
    }

    @Override
    public DocumentTypeFullDTO toComplexDto(DocumentType entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, DocumentTypeFullDTO.class);
    }

    @Override
    protected void initMapper() {
        super.initMapper();
        mapper.createTypeMap(DocumentType.class, DocumentTypeFullDTO.class)
                .setPostConverter(MappingContext::getDestination);
    }


    @Override
    protected void mapRequestToEntitySpecificFields(DocumentTypeRequest source, DocumentType destination) {
        destination.setFields(Objects.isNull(source.getFields()) ? new ArrayList<>() :
                fieldRepository.findAllById(source.getFields()));
    }

}
