package document_management.mapper.impl;

import document_management.entity.document.DocumentCard;
import document_management.entity.folder.Folder;
import document_management.entity.permission.PermissionGroup;
import document_management.entity.user.Position;
import document_management.entity.user.User;
import document_management.mapper.api.ComplexDtoMapper;
import document_management.protocol.dto.PermissionGroupDTO;
import document_management.protocol.dto.PermissionGroupFullDTO;
import document_management.protocol.request.PermissionGroupRequest;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Component
public class PermissionGroupMapper extends AbstractRequestDtoMapper<PermissionGroup, PermissionGroupDTO, PermissionGroupRequest>
        implements ComplexDtoMapper<PermissionGroup, PermissionGroupFullDTO> {

    @Autowired
    public PermissionGroupMapper(ModelMapper mapper) {
        super(mapper, PermissionGroup.class, PermissionGroupDTO.class, PermissionGroupRequest.class);
    }

    @Override
    public PermissionGroupFullDTO toComplexDto(PermissionGroup entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, PermissionGroupFullDTO.class);
    }

    @Override
    protected void initMapper() {
        super.initMapper();
        mapper.createTypeMap(PermissionGroup.class, PermissionGroupFullDTO.class)
                .setPostConverter(MappingContext::getDestination);
    }

    @Override
    protected void mapRequestToEntitySpecificFields(PermissionGroupRequest source, PermissionGroup destination) {
        destination.setMembers(Objects.isNull(source.getMembers()) ? new ArrayList<>() :
                source.getMembers().stream()
                        .map(User::new).collect(toList()));

        destination.setDocuments(Objects.isNull(source.getDocuments()) ? new ArrayList<>() :
                source.getDocuments().stream()
                        .map(DocumentCard::new).collect(toList()));

        destination.setFolders(Objects.isNull(source.getFolders()) ? new ArrayList<>() :
                source.getFolders().stream()
                        .map(Folder::new).collect(toList()));

        destination.setPositions(Objects.isNull(source.getPositions()) ? new ArrayList<>() :
                source.getPositions().stream()
                        .map(Position::new).collect(toList()));
    }

}