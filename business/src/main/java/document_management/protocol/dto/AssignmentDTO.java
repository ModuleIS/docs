package document_management.protocol.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AssignmentDTO {
    private Long id;
    private String text;
    private String term;
    private String type;
    private String status;
    private UserDto author;
    private UserDto performer;
}
