package document_management.protocol.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AuthenticationRequestDTO {
    @NotNull
    @NotBlank
    private String firstName;

    @NotNull
    @NotBlank
    private String secondName;

    @NotNull
    @NotBlank
    private String patronymic;

    @NotNull
    @NotBlank
    private String password;
}