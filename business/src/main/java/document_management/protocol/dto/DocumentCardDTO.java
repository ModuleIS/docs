package document_management.protocol.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DocumentCardDTO {

    private Long id;
    private String index;
    private String documentName;
    private DocumentTypeDTO documentType;
    private String documentStatus;
    private List<PermissionGroupDTO> permissionGroups = new ArrayList<>();
    private List<DocumentFieldValueDTO> documentFields = new ArrayList<>();
    private Integer version;
}
