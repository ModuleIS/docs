package document_management.protocol.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DocumentFieldDTO {
    private Long id;
    @NotNull
    @NotBlank
    private String name;

    private String type;
}
