package document_management.protocol.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DocumentFieldValueDTO {

    private String fieldName;
    private String type;
    private Object value;

}
