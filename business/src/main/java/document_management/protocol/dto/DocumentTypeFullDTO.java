package document_management.protocol.dto;

import lombok.Data;

import java.util.List;

@Data
public class DocumentTypeFullDTO {

    private Long id;
    private String name;
    private List<String> actions;
    private List<DocumentFieldDTO> fields;
    private List<TemplateDTO> templates;

}
