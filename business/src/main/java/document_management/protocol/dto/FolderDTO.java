package document_management.protocol.dto;

import lombok.Data;

@Data
public class FolderDTO {
    protected Long id;
    protected String name;
}
