package document_management.protocol.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class FolderFullDTO extends FolderDTO{

    private List<FolderFullDTO> subFolders;
}
