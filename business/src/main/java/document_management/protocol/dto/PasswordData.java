package document_management.protocol.dto;

import lombok.Data;

@Data
public class PasswordData {
    private String password;
    private String encodePassword;
}
