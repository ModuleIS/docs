package document_management.protocol.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PermissionGroupDTO {

    protected Long id;
    protected String name;
    protected String description;
}
