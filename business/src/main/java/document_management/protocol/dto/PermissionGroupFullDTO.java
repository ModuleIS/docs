package document_management.protocol.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PermissionGroupFullDTO extends PermissionGroupDTO{

    private String access;

    private List<PositionDTO> positions = new ArrayList<>();

    private List<UserDto> members = new ArrayList<>();

    private List<DocumentCardDTO> documents = new ArrayList<>();

    private List<FolderFullDTO> folders = new ArrayList<>();
}
