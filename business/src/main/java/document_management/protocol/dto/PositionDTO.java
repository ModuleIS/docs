package document_management.protocol.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PositionDTO {
    private Long id;
    @NotNull
    @NotBlank
    private String name;
}
