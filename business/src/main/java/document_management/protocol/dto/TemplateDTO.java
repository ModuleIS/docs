package document_management.protocol.dto;

import lombok.Data;

@Data
public class TemplateDTO {
    private Long id;
    private String name;
    private String key;
    private String extension;
    private String creationDate;

}
