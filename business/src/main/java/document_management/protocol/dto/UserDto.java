package document_management.protocol.dto;

import document_management.entity.user.UserRole;
import lombok.Data;

import java.time.Instant;

@Data
public class UserDto {
    private Long id;

    private String firstName;

    private String secondName;

    private String patronymic;

    private PositionDTO position;

    private UserRole role;

    private String phone;

    private String email;

    private String status;

    private boolean online;

    private boolean activate;

    private Instant creationDate;
}
