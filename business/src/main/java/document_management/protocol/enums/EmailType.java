package document_management.protocol.enums;

public enum EmailType {
    NEW_USER,
    CHANGE_EMAIL_ADDRESS,
    RESET_PASSWORD,
    FORGOT_PASSWORD,
    SEND_NEW_LETTER
}
