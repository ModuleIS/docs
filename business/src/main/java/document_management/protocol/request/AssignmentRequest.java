package document_management.protocol.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AssignmentRequest {
    private Long id;
    private String text;
    private String term;
    private String type;
    private String status;
    private Long authorId;
    private Long performerId;
    private Long documentId;
}
