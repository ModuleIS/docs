package document_management.protocol.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ChangeRoleRequest {
    @NotNull
    private Long id;
    @NotBlank
    private String role;
}
