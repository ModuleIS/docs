package document_management.protocol.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class ChangeStatusRequest {
    @NonNull
    private Long id;
    @NotBlank
    private String status;
}
