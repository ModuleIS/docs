package document_management.protocol.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
public class CreateDocumentCardRequest {

    private String index;
    private String documentName;
    private Long type;
    private Map<Long, String> documentFields = new HashMap<>();
    private List<Long> permissionGroups = new ArrayList<>();
}
