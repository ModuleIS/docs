package document_management.protocol.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@RequiredArgsConstructor
public class CreateUserRequest {

    @NotBlank
    private String firstName;

    @NotBlank
    private String secondName;

    @NotBlank
    private String patronymic;

    @NotNull
    private Long positionId;

    @NotBlank
    private String role;

    @NotBlank
    @Pattern(regexp = "[a-zA-Z0-9_]")
    private String password;

    @Pattern(regexp = "(^$|[0-9]{11})")
    private String phone;

    @Email
    @NotBlank
    private String email;
}
