package document_management.protocol.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class DocumentTypeRequest {

    private Long id;
    private String name;
    private List<String> actions;
    private List<Long> fields;

}
