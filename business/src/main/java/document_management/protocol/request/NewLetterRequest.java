package document_management.protocol.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@RequiredArgsConstructor
public class NewLetterRequest {

    @Email
    @NotBlank
    private String toEmail;

    @NotBlank
    private String text;
}
