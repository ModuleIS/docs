package document_management.protocol.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@RequiredArgsConstructor
public class PermissionGroupRequest {

    private Long id;
    @NotBlank
    @NotNull
    protected String name;
    protected String description;
    protected String access;
    protected List<Long> members = new ArrayList<>();
    protected List<Long> documents = new ArrayList<>();
    protected List<Long> folders = new ArrayList<>();
    protected List<Long> positions = new ArrayList<>();

}
