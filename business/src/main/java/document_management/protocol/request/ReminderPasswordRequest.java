package document_management.protocol.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ReminderPasswordRequest {
    @NotNull
    private Long id;
}
