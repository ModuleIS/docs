package document_management.protocol.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@RequiredArgsConstructor
public class ResetPasswordRequest {

    @Email
    @NotBlank
    private String email;
}
