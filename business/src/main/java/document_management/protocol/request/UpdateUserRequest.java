package document_management.protocol.request;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@RequiredArgsConstructor
public class UpdateUserRequest {
    @NonNull
    private Long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String secondName;

    @NotBlank
    private String patronymic;

    @NotBlank
    private Long positionId;

    @NotBlank
    private String role;

    @Pattern(regexp = "(^$|[0-9]{11})")
    private String phone;

    @Email
    @NotBlank
    private String email;
}
