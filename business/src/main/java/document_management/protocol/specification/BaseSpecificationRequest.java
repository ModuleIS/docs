package document_management.protocol.specification;

import org.springframework.data.jpa.domain.Specification;

public interface BaseSpecificationRequest<T> {

    Specification<T> getSpecification();
}
