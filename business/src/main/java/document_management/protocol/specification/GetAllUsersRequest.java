package document_management.protocol.specification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import document_management.entity.user.Position;
import document_management.entity.user.User;
import document_management.entity.user.UserRole;
import document_management.entity.user.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.Objects;

/**
 * Kostya Krivonos
 * 4/10/19
 * 5:10 PM
 */

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllUsersRequest implements BaseSpecificationRequest<User> {

    private String query;
    private UserRole role;
    private UserStatus status;
    private Position position;

    @Override
    public Specification<User> getSpecification() {
        return (Specification<User>) (root, query, criteriaBuilder) -> {
            Predicate mainPredicate;
            Predicate withUserData;
            Predicate withUserRole;
            Predicate withUserStatus;
            Predicate withUserPosition;
            Predicate withUserDataId;

            Join position = root.join("position");

            mainPredicate = criteriaBuilder.and(root.get("role").in(UserRole.MAIN_ADMIN).not());

            if (Objects.nonNull(this.getQuery()) && !this.getQuery().isEmpty()) {
                withUserData = criteriaBuilder.or(criteriaBuilder.like(root.get("firstName"), "%" + this.getQuery() + "%"),
                        criteriaBuilder.like(root.get("email"), "%" + this.getQuery() + "%"),
                        criteriaBuilder.like(root.get("secondName"), "%" + this.getQuery() + "%"),
                        criteriaBuilder.like(root.get("patronymic"), "%" + this.getQuery() + "%"),
                        criteriaBuilder.like(position.get("name"), "%" + this.getQuery() + "%"));

                mainPredicate = criteriaBuilder.and(mainPredicate, withUserData);
            }
            if (Objects.nonNull(this.getQuery()) && this.getQuery().isEmpty() && !this.getQuery().equals("")) {
                Long id = Long.parseLong(this.getQuery());
                withUserDataId = criteriaBuilder.or(root.get("id").in(id));

                mainPredicate = criteriaBuilder.and(mainPredicate, withUserDataId);
            }
            if (Objects.nonNull(this.getRole()) && this.getRole() != null) {
                withUserRole = criteriaBuilder.and(root.get("role").in(this.getRole()));
                mainPredicate = criteriaBuilder.and(mainPredicate, withUserRole);
            }
            if (Objects.nonNull(this.getStatus()) && this.getStatus() != null) {
                withUserStatus = criteriaBuilder.and(root.get("status").in(this.getStatus()));
                mainPredicate = criteriaBuilder.and(mainPredicate, withUserStatus);
            }
            if (Objects.nonNull(this.getPosition()) && this.getPosition() != null) {
                withUserPosition = criteriaBuilder.and(position.get("name").in(this.getPosition().getName()));
                mainPredicate = criteriaBuilder.and(mainPredicate, withUserPosition);
            }
            query.distinct(true);
            return mainPredicate;
        };
    }
}
