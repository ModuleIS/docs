package document_management.service.api;

import document_management.entity.assignment.Assignment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

public interface AssignmentService {
    Assignment createAssignment(Assignment newAssignment);

    Page<Assignment> getAllAssignments(Pageable pageable);

    void deleteById(long id);

    Assignment getAssignmentById(long id);
}
