package document_management.service.api;

import document_management.entity.document.DocumentField;

import java.util.List;

public interface DocumentFieldService {
    List<DocumentField> getAllFields();

    DocumentField saveField(DocumentField field);

    DocumentField getById(Long id);

    void deleteById(long id);

    boolean existsByName(String name);

    List<String> getAllFieldTypes();
}
