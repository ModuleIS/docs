package document_management.service.api;

import document_management.entity.document.DocumentCard;
import org.springframework.data.domain.Page;

public interface DocumentService {
    DocumentCard createDocument(DocumentCard document);

    Page<DocumentCard> getDocuments(int page, int limit);

    void archiveDocument(long id);

    DocumentCard getDocumentById(long id);
}
