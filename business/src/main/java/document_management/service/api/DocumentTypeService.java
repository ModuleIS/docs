package document_management.service.api;

import document_management.entity.document.DocumentType;
import net.bytebuddy.jar.asm.commons.Remapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DocumentTypeService {
    List<String> getAllDocumentActions();

    List<DocumentType> getAllDocumentTypes();

    void deleteTypeById(long id);

    boolean existsByName(String name);

    Page<DocumentType> getAllDocumentTypes(String filterByName, Pageable pageable);

    DocumentType saveType(DocumentType type);

    DocumentType getDocumentTypeById(long id);
}
