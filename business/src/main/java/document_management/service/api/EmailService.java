package document_management.service.api;

import document_management.protocol.enums.EmailType;

public interface EmailService {
    void sendEmailByType(EmailType type, Long userId, String username, String email, String text);
}
