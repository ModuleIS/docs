package document_management.service.api;

import document_management.entity.folder.Folder;

import java.util.List;

public interface FolderSystemService {
    List<Folder> getAllFolders();
}
