package document_management.service.api;

import document_management.protocol.dto.PasswordData;

public interface PasswordGenerator {
    PasswordData generatePassword();

    String encodePassword(String password);
}
