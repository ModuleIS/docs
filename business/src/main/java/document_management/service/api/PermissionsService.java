package document_management.service.api;

import document_management.entity.permission.PermissionGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PermissionsService {

    PermissionGroup createPermissionGroup(PermissionGroup request);

    PermissionGroup findById(Long id);

    void deletePermissionGroup(long id);

    Page<PermissionGroup> getAll(String filterByName, Pageable pageable);

    List<PermissionGroup> getAll();
}
