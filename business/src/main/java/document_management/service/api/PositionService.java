package document_management.service.api;

import document_management.entity.user.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PositionService {

    Page<Position> getAllPositions(String filterByName, Pageable pageable);
    
    Position getById(Long id);
    
    List<Position> getAll();

    boolean existByName(String name);

    Position savePosition(Position positionFromDB);

    void deleteById(long id);
}
