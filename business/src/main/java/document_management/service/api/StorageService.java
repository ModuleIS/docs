package document_management.service.api;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

    String saveFile (MultipartFile file);

    Resource load(String filename);

    boolean deleteFile(String key);
}
