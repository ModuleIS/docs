package document_management.service.api;

import document_management.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface UserService {
    User findById(Long id);
    User save (User user);
    User findBySecondName(String secondName);
    User createUser (User user);
    Boolean existUserByEmail(String email);
    Boolean existByPositionId(Long id);
    User findByFullName(String firstName,String secondName,String surname);
    Boolean existsByFirstNameAndSecondName(String firstName, String secondName);
    Page<User> getAllUserByFilter(Specification<User> userSpecification, Pageable pageable);
}
