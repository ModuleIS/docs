package document_management.service.impl;

import document_management.DBService.api.AssignmentDBService;
import document_management.entity.assignment.Assignment;
import document_management.service.api.AssignmentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AssignmentServiceImpl implements AssignmentService {
    private final AssignmentDBService assignmentDBService;

    @Override
    public Assignment createAssignment(Assignment assignment) {
        return assignmentDBService.createAssignment(assignment);
    }

    @Override
    public Page<Assignment> getAllAssignments(Pageable pageable) {
        return assignmentDBService.getAllAssignments(pageable);
    }

    @Override
    public void deleteById(long id) {
        assignmentDBService.deleteById(id);
    }

    @Override
    public Assignment getAssignmentById(long id) {
        return assignmentDBService.getAssignmentById(id);
    }
}
