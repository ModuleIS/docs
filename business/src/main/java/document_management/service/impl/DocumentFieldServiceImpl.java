package document_management.service.impl;

import document_management.DBService.api.DocumentFieldDBService;
import document_management.entity.document.DocumentField;
import document_management.service.api.DocumentFieldService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DocumentFieldServiceImpl implements DocumentFieldService {
    private final DocumentFieldDBService documentFieldDBService;

    @Override
    public List<DocumentField> getAllFields() {
        return documentFieldDBService.getAllFields();
    }

    @Override
    public DocumentField saveField(DocumentField field) {
        return documentFieldDBService.saveField(field);
    }

    @Override
    public DocumentField getById(Long id) {
        return documentFieldDBService.getById(id);
    }

    @Override
    public void deleteById(long id) {
        documentFieldDBService.deleteById(id);
    }

    @Override
    public boolean existsByName(String name) {
        return documentFieldDBService.existsByName(name);
    }

    @Override
    public List<String> getAllFieldTypes() {
        return documentFieldDBService.getAllFieldTypes();
    }
}
