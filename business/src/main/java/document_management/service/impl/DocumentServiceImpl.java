package document_management.service.impl;

import document_management.DBService.api.DocumentCardDBService;
import document_management.entity.document.DocumentCard;
import document_management.entity.document.DocumentStatus;
import document_management.service.api.DocumentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
    public class DocumentServiceImpl implements DocumentService {

    private DocumentCardDBService documentCardDBService;

    @Override
    public DocumentCard createDocument(DocumentCard document) {
        return documentCardDBService.saveDocument(document);
    }

    @Override
    public  Page<DocumentCard> getDocuments(int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by("id"));
        return documentCardDBService.getDocuments(pageRequest);
    }

    @Override
    public void archiveDocument(long id) {
        documentCardDBService.setDocumentStatus(id, DocumentStatus.ARCHIVE);
    }

    @Override
    public DocumentCard getDocumentById(long id) {
        return documentCardDBService.getDocumentById(id);
    }
}
