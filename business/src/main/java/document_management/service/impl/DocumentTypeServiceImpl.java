package document_management.service.impl;

import document_management.DBService.api.DocumentTypeDBService;
import document_management.entity.document.DocumentType;
import document_management.service.api.DocumentTypeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class DocumentTypeServiceImpl implements DocumentTypeService{

    private final DocumentTypeDBService documentTypeDBService;

    @Override
    public List<String> getAllDocumentActions() {
        return documentTypeDBService.getAllActions();
    }

    @Override
    public List<DocumentType> getAllDocumentTypes() {
        return documentTypeDBService.getAllDocumentTypes();
    }

    @Override
    public void deleteTypeById(long id) {
        documentTypeDBService.deleteTypeById(id);
    }

    @Override
    public boolean existsByName(String name) {
        return documentTypeDBService.existsByName(name);
    }

    @Override
    public Page<DocumentType> getAllDocumentTypes(String filterByName, Pageable pageable) {
        Specification<DocumentType> specification = getSpecification(filterByName);
        return documentTypeDBService.getAllDocumentTypes(specification, pageable);
    }

    @Override
    public DocumentType saveType(DocumentType type) {
        return documentTypeDBService.save(type);
    }

    @Override
    public DocumentType getDocumentTypeById(long id) {
        return documentTypeDBService.getDocumentTypeById(id);
    }


    private Specification<DocumentType> getSpecification(String filterByName) {
        return (Specification<DocumentType>) (root, criteriaQuery, cb) -> {
            Predicate predicate = cb.conjunction();
            if(Objects.nonNull(filterByName) && !filterByName.isEmpty()){
                predicate = cb.like(cb.upper(root.get("name")), "%" + String.valueOf(filterByName).toUpperCase() +"%");
            }
            return predicate;
        };
    }
}
