package document_management.service.impl;

import document_management.protocol.enums.EmailType;
import document_management.service.api.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.mail.javamail.MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {


    private final String FORGOT_PASSWORD_EMAIL_SUBJECT = "Password reminder";
    private final String NEW_USER_EMAIL_SUBJECT = "New user in system";
    private final String SEND_NEW_LETTER_SUBJECT = "New letter";

    private final String FORGOT_PASSWORD_EMAIL_TEMPLATE = "forgot_pass";
    private final String NEW_USER_EMAIL_TEMPLATE = "welcome_mail";
    private final String NEW_LETTER_EMAIL_TEMPLATE = "new_letter_mail";

    private final String HEAD_VARIABLE = "head";
    private final String TEXT_VARIABLE = "text";

    private final String TEXT_FOR_NEW_USER_EMAIL = "";

    private final SpringTemplateEngine templateEngine;
    private final JavaMailSender emailSender;

    //    @Value("${email.changePasswordUrl}")
//    private String LINK_CHANGE_PASSWORD;
    @Value("${spring.mail.username}")
    private String SUPPORT_EMAIL;

    @Override
    public void sendEmailByType(EmailType type, Long userId, String username, String email, String text) {
        switch (type) {
            case NEW_USER:
                buildAndSend(username, email, text, NEW_USER_EMAIL_SUBJECT, NEW_USER_EMAIL_TEMPLATE);
                break;
            case FORGOT_PASSWORD:
                buildAndSend(username, email, text, FORGOT_PASSWORD_EMAIL_SUBJECT, FORGOT_PASSWORD_EMAIL_TEMPLATE);
                break;
            case SEND_NEW_LETTER:
                buildAndSend(username, email, text, SEND_NEW_LETTER_SUBJECT, NEW_LETTER_EMAIL_TEMPLATE);
                break;
        }
    }

    private void buildAndSend(String username, String email, String text, String emailSubject, String emailTemplate) {
        HashMap<String, String> variablesAndValuesForHtmlContext = new HashMap<>();
        variablesAndValuesForHtmlContext.put(HEAD_VARIABLE, "Hello " + username + ",");
        if(text != null)variablesAndValuesForHtmlContext.put(TEXT_VARIABLE, text);
        sendEmail(email, emailSubject, emailTemplate, variablesAndValuesForHtmlContext);
    }

    private void sendEmail(String emailTo, String emailSubject, String emailTemplateName, HashMap<String, String> variablesAndValuesForHtmlContext) {
        Context context = new Context();
        variablesAndValuesForHtmlContext.forEach(context::setVariable);
        String html = templateEngine.process(emailTemplateName, context);
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, MULTIPART_MODE_MIXED_RELATED, UTF_8.name());
            helper.setText(html, true);
            helper.setFrom(SUPPORT_EMAIL);
            helper.setTo(emailTo);
            message.setSubject(emailSubject);
            emailSender.send(message);
            log.info("[x] The email was sent successfully, subject:[{}].", emailSubject);
        } catch (MessagingException | MailException e) {
            log.error("[x] Error while sending email for subject:[{}], error message:[{}].", emailSubject, e.getMessage());
        }
    }
}
