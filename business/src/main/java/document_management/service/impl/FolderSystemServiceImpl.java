package document_management.service.impl;

import document_management.DBService.api.FolderDBService;
import document_management.entity.folder.Folder;
import document_management.service.api.FolderSystemService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FolderSystemServiceImpl implements FolderSystemService {

    private final FolderDBService folderDBService;
    @Override
    public List<Folder> getAllFolders() {
        return folderDBService.getRootFolders();
    }
}
