package document_management.service.impl;

import document_management.protocol.dto.PasswordData;
import document_management.service.api.PasswordGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class PasswordGeneratorImpl implements PasswordGenerator {
    private final PasswordEncoder passwordEncoder;

    @Override
    public PasswordData generatePassword() {
        final int length = 8;

        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        SecureRandom random = new SecureRandom();
        String sb = IntStream.range(0, length)
                .map(i -> random.nextInt(chars.length()))
                .mapToObj(randomIndex -> String.valueOf(chars.charAt(randomIndex)))
                .collect(Collectors.joining());

        PasswordData data = new PasswordData();
        data.setPassword(sb);
        data.setEncodePassword(passwordEncoder.encode(sb));
        return data;
    }

    @Override
    public String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }
}
