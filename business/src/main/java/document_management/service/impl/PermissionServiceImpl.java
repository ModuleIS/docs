package document_management.service.impl;

import document_management.DBService.api.PermissionDBService;
import document_management.entity.permission.PermissionGroup;
import document_management.service.api.PermissionsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class PermissionServiceImpl implements PermissionsService {

    private final PermissionDBService permissionDBService;

    @Override
    public PermissionGroup createPermissionGroup(PermissionGroup permissionGroup) {
        return permissionDBService.save(permissionGroup);
    }

    @Override
    public PermissionGroup findById(Long id) {
        return permissionDBService.findById(id);
    }

    @Override
    public void deletePermissionGroup(long id) {
        PermissionGroup permissionGroup = permissionDBService.findById(id);
        permissionDBService.deletePermissionGroup(permissionGroup);
    }

    @Override
    public Page<PermissionGroup> getAll(String filterByName, Pageable pageable) {
        Specification<PermissionGroup> specification = getSpecification(filterByName);
        return permissionDBService.findAll(specification, pageable);
    }

    @Override
    public List<PermissionGroup> getAll() {
        return permissionDBService.getAll();
    }

    private Specification<PermissionGroup> getSpecification(String filterByName) {
        return (Specification<PermissionGroup>) (root, criteriaQuery, cb) -> {
            Predicate predicate = cb.conjunction();
            if(Objects.nonNull(filterByName) && !filterByName.isEmpty()){
               predicate = cb.like(cb.upper(root.get("name")), "%" + String.valueOf(filterByName).toUpperCase() +"%");
            }
            return predicate;
        };
    }
}
