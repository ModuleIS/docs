package document_management.service.impl;

import document_management.DBService.api.PositionDBService;
import document_management.entity.user.Position;
import document_management.service.api.PositionService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class PositionServiceImpl implements PositionService {

    private final PositionDBService positionDBService;

    @Override
    public Page<Position> getAllPositions(String filterByName, Pageable pageable) {
        Specification<Position> specification = getSpecification(filterByName);
        return positionDBService.getAllPositions(specification, pageable);
    }

    @Override
    public Position getById(Long id) {
        return positionDBService.getById(id);
    }

    @Override
    public List<Position> getAll() {
        return positionDBService.getAll();
    }

    @Override
    public boolean existByName(String name) {
        return positionDBService.existsByName(name);
    }

    @Override
    public Position savePosition(Position position) {
        return positionDBService.save(position);
    }

    @Override
    public void deleteById(long id) {
        positionDBService.deleteById(id);
    }

    private Specification<Position> getSpecification(String filterByName) {
        return (Specification<Position>) (root, criteriaQuery, cb) -> {
            Predicate predicate = cb.conjunction();
            if(Objects.nonNull(filterByName) && !filterByName.isEmpty()){
                predicate = cb.like(cb.upper(root.get("name")), "%" + String.valueOf(filterByName).toUpperCase() +"%");
            }
            return predicate;
        };
    }
}
