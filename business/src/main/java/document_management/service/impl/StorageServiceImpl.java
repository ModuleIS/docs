package document_management.service.impl;

import document_management.service.api.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static java.util.Objects.nonNull;

@Slf4j
@Service
public class StorageServiceImpl implements StorageService {

    @Value("${documentContent.uploadBase}")
    private String uploadBase;
    @Value("${documentContent.uploadDocumentPath}")
    private String uploadDocumentFolder;

    private Path directory;

    @PostConstruct
    public void initDirectory() {
        try {
            Path path = Paths.get(uploadBase + uploadDocumentFolder);
            if(!Files.exists(path)){
                this.directory = Files.createDirectories(path);
            } else {
                this.directory = path;
            }
        } catch (Exception e) {
            log.error("Can't created directory for media! Error : {}", e.getMessage());
        }
    }

    @Override
    public String saveFile(MultipartFile file) {
        if (nonNull(directory) && nonNull(file)) {
            try {
                String uuidFirst = UUID.randomUUID().toString();
                String uuidSecond = UUID.randomUUID().toString();

                String resultFilename = uuidFirst + "." + uuidSecond + "."+ file.getOriginalFilename();
                Path path = Paths.get(directory.toString() + File.separator + resultFilename);
                Files.write(path, file.getBytes());
                return resultFilename;
            } catch (IOException e) {
                log.error("Error during saving Media file: {}", e.getMessage());
            }
        }
        return null;
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = Paths.get(directory.toAbsolutePath().toString(), filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public boolean deleteFile(String key) {
        Path file = Paths.get(directory.toAbsolutePath().toString(), key);
        return FileUtils.deleteQuietly(file.toFile());
    }

}
