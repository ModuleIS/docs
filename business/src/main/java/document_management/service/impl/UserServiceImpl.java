package document_management.service.impl;

import document_management.DBService.api.UserDBService;
import document_management.entity.user.User;
import document_management.service.api.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDBService userDBService;

    @Override
    public User findById(Long id) {
        return userDBService.findById(id);
    }

    @Override
    public User save(User user) {
        return userDBService.save(user);
    }

    @Override
    public User findBySecondName(String secondName) {
        return userDBService.findBySecondName(secondName);
    }

    @Override
    public User createUser(User user) {
        return userDBService.createUser(user);
    }

    @Override
    public Boolean existUserByEmail(String email) {
        return userDBService.existUserByEmail(email);
    }

    @Override
    public Boolean existByPositionId(Long id) {
        return userDBService.existByPosition(id);
    }

    @Override
    public User findByFullName(String firstName, String secondName, String patronymic) {
        return userDBService.findByFullName(firstName, secondName, patronymic);
    }

    @Override
    public Boolean existsByFirstNameAndSecondName(String firstName, String secondName) {
        return userDBService.existsByFirstNameAndSecondName(firstName, secondName);
    }

    @Override
    public Page<User> getAllUserByFilter(Specification<User> userSpecification, Pageable pageable){
        return userDBService.findAll(userSpecification, pageable);
    }

}
