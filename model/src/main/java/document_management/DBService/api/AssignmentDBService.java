package document_management.DBService.api;

import document_management.entity.assignment.Assignment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AssignmentDBService {
    Assignment createAssignment(Assignment assignment);

    Page<Assignment> getAllAssignments(Pageable pageable);

    void deleteById(long id);

    Assignment getAssignmentById(long id);
}
