package document_management.DBService.api;

import document_management.entity.document.DocumentCard;
import document_management.entity.document.DocumentStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface DocumentCardDBService {
    DocumentCard saveDocument(DocumentCard document);

    Page<DocumentCard> getDocuments(PageRequest pageRequest);

    void setDocumentStatus(long id, DocumentStatus archive);

    DocumentCard getDocumentById(long id);
}
