package document_management.DBService.api;

import document_management.entity.document.DocumentType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface DocumentTypeDBService {
    List<String> getAllActions();

    List<DocumentType> getAllDocumentTypes();

    void deleteTypeById(long id);

    boolean existsByName(String name);

    Page<DocumentType> getAllDocumentTypes(Specification<DocumentType> specification, Pageable pageable);

    DocumentType save(DocumentType type);

    DocumentType getDocumentTypeById(long id);
}
