package document_management.DBService.api;

import document_management.entity.folder.Folder;

import java.util.List;

public interface FolderDBService {

    List<Folder> getRootFolders();
}
