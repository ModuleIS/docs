package document_management.DBService.api;

import document_management.entity.permission.PermissionGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface PermissionDBService {
    PermissionGroup save(PermissionGroup permissionGroup);

    PermissionGroup findById(Long id);

    void deletePermissionGroup(PermissionGroup permissionGroup);

    Page<PermissionGroup> findAll(Specification<PermissionGroup> specification, Pageable pageable);

    List<PermissionGroup> getAll();
}
