package document_management.DBService.api;

import document_management.entity.user.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface PositionDBService {

    Page<Position> getAllPositions(Specification<Position> specification, Pageable pageable);

    Position getById(Long id);

    List<Position> getAll();

    boolean existsByName(String name);

    Position save(Position position);

    void deleteById(long id);
}
