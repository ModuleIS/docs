package document_management.DBService.api;

import document_management.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface UserDBService {
    User findById(Long id);

    User save(User user);

    User findBySecondName(String secondName);

    User createUser(User user);

    Boolean existUserByEmail(String email);

    Boolean existByPosition(Long id);

    User findByFullName(String firstName, String secondName, String surname);

    Boolean existsByFirstNameAndSecondName(String firstName, String secondName);

    Page<User> findAll(Specification<User> userSpecification, Pageable pageable);
}
