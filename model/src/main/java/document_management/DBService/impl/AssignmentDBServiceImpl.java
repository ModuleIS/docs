package document_management.DBService.impl;

import document_management.DBService.api.AssignmentDBService;
import document_management.entity.assignment.Assignment;
import document_management.entity.document.DocumentCard;
import document_management.exeption.ResourceNotFoundException;
import document_management.repository.AssignmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AssignmentDBServiceImpl implements AssignmentDBService {

    private final AssignmentRepository assignmentRepository;

    @Override
    public Assignment createAssignment(Assignment assignment) {
        return assignmentRepository.save(assignment);
    }

    @Override
    public Page<Assignment> getAllAssignments(Pageable pageable) {
        return assignmentRepository.findAll(pageable);
    }

    @Override
    public void deleteById(long id) {
        assignmentRepository.deleteById(id);
    }

    @Override
    public Assignment getAssignmentById(long id) {
        return assignmentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(Assignment.class.getSimpleName() +
                        " is not exist with id = " + id));
    }
}
