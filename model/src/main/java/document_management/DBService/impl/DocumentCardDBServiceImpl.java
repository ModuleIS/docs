package document_management.DBService.impl;

import document_management.DBService.api.DocumentCardDBService;
import document_management.entity.document.DocumentCard;
import document_management.entity.document.DocumentStatus;
import document_management.exeption.ResourceNotFoundException;
import document_management.repository.DocumentCardRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
@AllArgsConstructor
public class DocumentCardDBServiceImpl implements DocumentCardDBService {

    private DocumentCardRepository documentCardRepository;

    @Override
    public DocumentCard saveDocument(DocumentCard document) {
        return documentCardRepository.save(document);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DocumentCard> getDocuments(PageRequest pageRequest) {
        return documentCardRepository.findAll(pageRequest);
    }

    @Override
    public void setDocumentStatus(long id, DocumentStatus status) {
        DocumentCard documentCard = documentCardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(DocumentCard.class.getSimpleName() +
                        " is not exist with id = " + id));
        documentCard.setDocumentStatus(status);
    }

    @Override
    public DocumentCard getDocumentById(long id) {
        return documentCardRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(DocumentCard.class.getSimpleName() +
                        " is not exist with id = " + id));
    }
}
