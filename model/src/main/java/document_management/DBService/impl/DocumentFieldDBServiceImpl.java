package document_management.DBService.impl;

import document_management.DBService.api.DocumentFieldDBService;
import document_management.entity.document.DocumentField;
import document_management.entity.document.FieldType;
import document_management.exeption.ResourceNotFoundException;
import document_management.repository.DocumentFieldRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DocumentFieldDBServiceImpl implements DocumentFieldDBService {

    private final DocumentFieldRepository documentFieldRepository;

    @Override
    public List<DocumentField> getAllFields() {
        return documentFieldRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public DocumentField saveField(DocumentField field) {
        return documentFieldRepository.save(field);
    }

    @Override
    public DocumentField getById(Long id) {
        return documentFieldRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(DocumentField.class.getSimpleName() +
                        " is not exist with id = " + id));
    }

    @Override
    public void deleteById(long id) {
        documentFieldRepository.deleteById(id);
    }

    @Override
    public boolean existsByName(String name) {
        return documentFieldRepository.existsByName(name);
    }

    @Override
    public List<String> getAllFieldTypes() {
        return Arrays.stream(FieldType.values())
                .map(FieldType::name)
                .collect(Collectors.toList());
    }
}
