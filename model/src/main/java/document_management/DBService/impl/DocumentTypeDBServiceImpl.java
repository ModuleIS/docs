package document_management.DBService.impl;

import document_management.DBService.api.DocumentTypeDBService;
import document_management.entity.document.DocumentAction;
import document_management.entity.document.DocumentType;
import document_management.exeption.ResourceNotFoundException;
import document_management.repository.DocumentTypeRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DocumentTypeDBServiceImpl implements DocumentTypeDBService {

    private final DocumentTypeRepository repository;

    @Override
    public List<String> getAllActions() {
        return Arrays.stream(DocumentAction.values())
                .map(DocumentAction::name)
                .collect(Collectors.toList());
    }

    @Override
    public List<DocumentType> getAllDocumentTypes() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public void deleteTypeById(long id) {
        repository.deleteById(id);
    }

    @Override
    public boolean existsByName(String name) {
        return repository.existsByName(name);
    }

    @Override
    public Page<DocumentType> getAllDocumentTypes(Specification<DocumentType> specification, Pageable pageable) {
        return repository.findAll(specification, pageable);
    }

    @Override
    public DocumentType save(DocumentType type) {
        return repository.save(type);
    }

    @Override
    public DocumentType getDocumentTypeById(long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(DocumentType.class.getSimpleName() +
                        " is not exist with id = " + id));
    }
}
