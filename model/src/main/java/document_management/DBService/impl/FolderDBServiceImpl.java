package document_management.DBService.impl;

import document_management.DBService.api.FolderDBService;
import document_management.entity.folder.Folder;
import document_management.repository.FolderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FolderDBServiceImpl implements FolderDBService {

    private final FolderRepository repository;

    @Override
    public List<Folder> getRootFolders() {
        return repository.getRootFolders();
    }
}
