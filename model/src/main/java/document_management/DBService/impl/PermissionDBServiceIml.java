package document_management.DBService.impl;

import document_management.DBService.api.PermissionDBService;
import document_management.entity.permission.PermissionGroup;
import document_management.exeption.ResourceNotFoundException;
import document_management.repository.PermissionsRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor

public class PermissionDBServiceIml implements PermissionDBService {

    private final PermissionsRepository permissionsRepository;

    @Override
    public PermissionGroup save(PermissionGroup permissionGroup) {
        return permissionsRepository.save(permissionGroup);
    }

    @Override
    public PermissionGroup findById(Long id) {
        return permissionsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(PermissionGroup.class.getSimpleName() +
                        " is not exist with id = " + id));
    }

    @Override
    public void deletePermissionGroup(PermissionGroup permissionGroup) {
        permissionsRepository.delete(permissionGroup);
    }

    @Override
    public Page<PermissionGroup> findAll(Specification<PermissionGroup> specification, Pageable pageable) {
        return permissionsRepository.findAll(specification, pageable);
    }

    @Override
    public List<PermissionGroup> getAll() {
        return permissionsRepository.findAll(Sort.by(Sort.Direction.ASC,"name"));
    }


}
