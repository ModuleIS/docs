package document_management.DBService.impl;

import document_management.DBService.api.PositionDBService;
import document_management.entity.user.Position;
import document_management.exeption.ResourceNotFoundException;
import document_management.repository.PositionRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PositionDBServiceImpl implements PositionDBService {

    private final PositionRepository positionRepository;

    @Override
    public Page<Position> getAllPositions(Specification<Position> specification, Pageable pageable) {
        return positionRepository.findAll(specification, pageable);
    }

    @Override
    public Position getById(Long id) {
        return positionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(Position.class.getSimpleName() +
                        " is not exist with id = " + id));
    }

    @Override
    public List<Position> getAll() {
        return positionRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public boolean existsByName(String name) {
        return positionRepository.existsByName(name);
    }

    @Override
    public Position save(Position position) {
        return positionRepository.save(position);
    }

    @Override
    public void deleteById(long id) {
        positionRepository.deleteById(id);
    }
}
