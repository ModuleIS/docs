package document_management.DBService.impl;

import document_management.DBService.api.UserDBService;
import document_management.entity.user.User;
import document_management.exeption.UserNotFoundException;
import document_management.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserDBServiceImpl implements UserDBService {
    private final UserRepository userRepository;

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(() ->
                new UserNotFoundException("User doesn't exists"));
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findBySecondName(String secondName) {
        return userRepository.findBySecondName(secondName).orElseThrow(() ->
                new UserNotFoundException("User doesn't exists"));
    }

    public User findByFullName(String firstName, String secondName, String patronymic) {
        return userRepository.findByFirstNameAndSecondNameAndPatronymic(firstName, secondName, patronymic)
                .orElseThrow(() ->
                        new UserNotFoundException("User doesn't exists"));
    }

    @Override
    public Boolean existsByFirstNameAndSecondName(String firstName, String secondName) {
        return userRepository.existsByFirstNameAndSecondName(firstName, secondName);
    }

    @Override
    public Page<User> findAll(Specification<User> userSpecification, Pageable pageable) {
        return userRepository.findAll(userSpecification, pageable);
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Boolean existUserByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Boolean existByPosition(Long id) {
        return userRepository.existsByPosition_Id(id);
    }
}
