package document_management.entity.assignment;


import document_management.entity.document.DocumentCard;
import document_management.entity.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "assignment")
@Getter
@Setter
public class Assignment {
    @Id
    @SequenceGenerator(name = "ASSIGNMENTS_ID_GENERATOR", sequenceName = "ASSIGNMENTS_ID_SEQ",  allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSIGNMENTS_ID_GENERATOR")
    private Long id;

    @Enumerated(EnumType.STRING)
    private AssignmentType type;

    @Enumerated(EnumType.STRING)
    private AssignmentStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    private User author;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "performer_id")
    private User performer;

    private LocalDateTime term;

    @Column(length = 3000, columnDefinition = "TEXT")
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id")
    private DocumentCard document;

}
