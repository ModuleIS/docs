package document_management.entity.assignment;

public enum AssignmentStatus {
    OPEN, ON_WORK, ON_CHECK, CLOSE
}
