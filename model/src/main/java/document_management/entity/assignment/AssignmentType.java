package document_management.entity.assignment;

public enum AssignmentType {
     PERFORM, ACQUAINTANCE, SIGNATURE
}
