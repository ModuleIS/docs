package document_management.entity.document;

public enum DocumentAction {
    ACQUAINTANCE, EDIT, SIGN
}
