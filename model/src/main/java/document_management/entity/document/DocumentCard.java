package document_management.entity.document;

import document_management.entity.assignment.Assignment;
import document_management.entity.permission.PermissionGroup;
import document_management.entity.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.w3c.dom.Document;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class DocumentCard {
    @Id
    @SequenceGenerator(name = "DOC_CARDS_ID_GENERATOR", sequenceName = "DOC_CARDS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOC_CARDS_ID_GENERATOR")
    private Long id;

//    @CreationTimestamp
//    @Column(updatable = false, nullable = false)
//    private LocalDateTime creationDate;
//
    private String index;
//
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "author_id")
//    private User author;

    @ElementCollection
    @CollectionTable(name = "document_card_field_mapping",
            joinColumns = {@JoinColumn(name = "document_id", referencedColumnName = "id")})
    @MapKeyJoinColumn(name = "field_id")
    @Column(name = "value")
    private Map<DocumentField, String> documentFields;

    @ManyToOne
    private DocumentType type;

//    @Column(length = 3000, columnDefinition = "TEXT")
//    private String contents;

    @Column(length = 100, nullable = false)
    private String documentName;

    @Enumerated(EnumType.STRING)
    private DocumentStatus documentStatus;

//    private LocalDateTime endDate;
//
//    private LocalDateTime closeDate;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "close_initiator_id")
//    private User closeInitiator;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<PermissionGroup> permissionGroups = new ArrayList<>();

//    @ManyToMany(fetch = FetchType.LAZY)
//    private List<DocumentCard> relatedDocuments = new ArrayList<>();

//    @OneToMany(fetch = FetchType.LAZY)
//    private List<DocumentVersion> versions = new ArrayList<>();

//    @ElementCollection
//    private List<String> attachments = new ArrayList<>();

//    @OneToMany(mappedBy = "documentCard")
//    private List<Assignment> route = new ArrayList<>();

//    @ManyToMany
//    private List<User> familiarized = new ArrayList<>();

    public DocumentCard(Long id) {
        this.id = id;
    }
}
