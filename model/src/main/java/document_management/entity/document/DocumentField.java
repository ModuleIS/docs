package document_management.entity.document;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class DocumentField {

    @Id
    @SequenceGenerator(name = "DOC_FIELDS_ID_GENERATOR", sequenceName = "DOC_FIELDS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOC_FIELDS_ID_GENERATOR")
    private Long id;

    @Column(unique=true)
    private String name;

    @Enumerated(EnumType.STRING)
    private FieldType type;
}
