package document_management.entity.document;

public enum DocumentStatus {
    NEW, ARCHIVE
}
