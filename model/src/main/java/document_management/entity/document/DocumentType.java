package document_management.entity.document;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class DocumentType {

    @Id
    @SequenceGenerator(name = "DOC_TYPES_ID_GENERATOR", sequenceName = "DOC_TYPES_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOC_TYPES_ID_GENERATOR")
    private Long id;

    @Column(unique=true)
    private String name;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<DocumentAction> actions;

    @ManyToMany
    private List<DocumentField> fields;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Template> templates;

    public DocumentType(Long id) {
        this.id = id;
    }
}
