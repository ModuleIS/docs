package document_management.entity.document;

import document_management.entity.user.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class DocumentVersion {

    @Id
    @SequenceGenerator(name = "DOC_VERS_ID_GENERATOR", sequenceName = "DOC_VERS_ID_SEQ",  allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOC_VERS_ID_GENERATOR")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    private User author;

    @Column(nullable = false)
    private int version = 1;

    @PreUpdate
    private void updateVersion(){
        this.version += 1;
    }

    private  String file;

    @CreationTimestamp
    private LocalDateTime versionDate = LocalDateTime.now();

}
