package document_management.entity.document;

public enum FieldType {
    NUMBER, STRING, TEXT, DATE, USER
}
