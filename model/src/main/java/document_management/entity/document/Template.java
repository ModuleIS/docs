package document_management.entity.document;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode
public class Template {

    @Id
    @SequenceGenerator(name = "DOC_TEMPLATES_ID_GENERATOR", sequenceName = "DOC_TEMPLATES_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOC_TEMPLATES_ID_GENERATOR")
    private Long id;

    private String name;

    private String extension;

    private String key;

    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime creationDate;

}
