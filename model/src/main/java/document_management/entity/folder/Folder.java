package document_management.entity.folder;

import document_management.entity.document.DocumentCard;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Folder {

    @Id
    @SequenceGenerator(name = "FOLDERS_ID_GENERATOR", sequenceName = "FOLDERS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FOLDERS_ID_GENERATOR")
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private Folder parent;

    @OneToMany(mappedBy = "parent")
    private List<Folder> subfolders = new ArrayList<>();

    @OneToMany
    private List<DocumentCard> documents = new ArrayList<>();

    public Folder(Long id) {
        this.id = id;
    }
}
