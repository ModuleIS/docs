package document_management.entity.permission;

public enum PermissionAccess {
    READ, WRITE
}
