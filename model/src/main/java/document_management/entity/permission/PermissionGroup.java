package document_management.entity.permission;


import document_management.entity.document.DocumentCard;
import document_management.entity.folder.Folder;
import document_management.entity.user.Position;
import document_management.entity.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PermissionGroup {

    @Id
    @SequenceGenerator(name = "PER_GROUPS_ID_GENERATOR", sequenceName = "PER_GROUPS_ID_SEQ",  allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PER_GROUPS_ID_GENERATOR")
    private Long id;

    @Column(length = 100)
    private String name;

    @Column(columnDefinition = "TEXT", length = 1000)
    private String description;

    @Enumerated(EnumType.STRING)
    private PermissionAccess access;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Position> positions = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> members = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    private List<DocumentCard> documents = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Folder> folders = new ArrayList<>();

    public PermissionGroup(Long id) {
        this.id = id;
    }
}
