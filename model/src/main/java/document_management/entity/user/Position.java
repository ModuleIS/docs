package document_management.entity.user;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Position {

    @Id
    @SequenceGenerator(name = "POSITIONS_ID_GENERATOR", sequenceName = "POSITIONS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "POSITIONS_ID_GENERATOR")
    private Long id;

    @Column
    private String name;

    public Position(Long id) {
        this.id = id;
    }
}
