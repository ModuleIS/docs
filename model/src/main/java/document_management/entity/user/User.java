package document_management.entity.user;

import document_management.entity.folder.Folder;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.beans.Encoder;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public class User {

    @Id
    @SequenceGenerator(name = "USERS_ID_GENERATOR", sequenceName = "USERS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_ID_GENERATOR")
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name", nullable = false, length = 100)
    private String firstName;

    @Column(name = "second_name", nullable = false, length = 100)
    private String secondName;

    @Column(name = "patronymic", nullable = false, length = 100)
    private String patronymic;

    @ManyToOne
    private Position position;

    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    private String phone;

    @Column(unique = true)
    private String email;

    @Column(nullable = false)
    private boolean online;

    @Column(nullable = false)
    private boolean activate;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private Instant creationDate;

    @OneToMany
    private List<Folder> folders = new ArrayList<>();

    public User(Long id) {
        this.id = id;
    }
}
