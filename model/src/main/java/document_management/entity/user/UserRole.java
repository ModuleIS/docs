package document_management.entity.user;

public enum UserRole {

    MAIN_ADMIN, ADMIN, USER;

}
