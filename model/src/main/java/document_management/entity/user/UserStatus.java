package document_management.entity.user;

public enum UserStatus {

    INVITATION_SENT, ACTIVE, DEACTIVATED, PASSWORD_RESTORE;
}
