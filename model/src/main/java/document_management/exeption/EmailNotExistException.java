package document_management.exeption;

public class EmailNotExistException extends RuntimeException{
    public EmailNotExistException() {
        super("The user with this email does not exist in the system.");
    }
}
