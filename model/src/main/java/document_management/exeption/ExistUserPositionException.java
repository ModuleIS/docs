package document_management.exeption;

public class ExistUserPositionException extends RuntimeException{
    public ExistUserPositionException() {
        super("You cannot delete a post, the post is linked to the user.");
    }
}
