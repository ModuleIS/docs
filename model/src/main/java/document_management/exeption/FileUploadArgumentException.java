package document_management.exeption;

public class FileUploadArgumentException extends RuntimeException{

    public FileUploadArgumentException(String message) {
        super(message);
    }
}
