package document_management.exeption;

public class ResourceDeleteException extends RuntimeException {

    public ResourceDeleteException(String message) {
        super(message);
    }
}
