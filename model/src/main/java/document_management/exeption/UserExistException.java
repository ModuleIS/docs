package document_management.exeption;

public class UserExistException extends RuntimeException{
    public UserExistException(String message) {
        super(message);
    }
}
