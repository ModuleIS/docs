package document_management.exeption;

public class UserRoleException extends RuntimeException{
    public UserRoleException() {
        super("The user already has this role.");
    }
}
