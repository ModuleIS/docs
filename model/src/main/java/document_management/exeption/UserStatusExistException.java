package document_management.exeption;

public class UserStatusExistException extends RuntimeException{
    public UserStatusExistException() {
        super("User already has this status.");
    }
}
