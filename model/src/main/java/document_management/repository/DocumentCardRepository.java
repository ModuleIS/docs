package document_management.repository;

import document_management.entity.document.DocumentCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentCardRepository extends JpaRepository<DocumentCard, Long> {
}
