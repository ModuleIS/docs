package document_management.repository;

import document_management.entity.document.DocumentField;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentFieldRepository extends JpaRepository<DocumentField, Long> {

    boolean existsByName(String name);
}
