package document_management.repository;

import document_management.entity.folder.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FolderRepository extends JpaRepository<Folder, Long> {

    @Query(value = "SELECT f FROM  Folder f WHERE f.parent is NULL")
    List<Folder> getRootFolders();
}
