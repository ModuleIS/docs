package document_management.repository;

import document_management.entity.permission.PermissionGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionsRepository extends JpaRepository<PermissionGroup, Long> {

    Page<PermissionGroup> findAll(Specification<PermissionGroup> example, Pageable pageable);
}
