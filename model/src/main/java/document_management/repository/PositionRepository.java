package document_management.repository;

import document_management.entity.user.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Long> {

    Page<Position> findAll(Specification<Position> specification, Pageable pageable);

    boolean existsByName(String name);
}
