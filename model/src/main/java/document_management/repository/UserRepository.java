package document_management.repository;

import document_management.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

    Optional<User> findBySecondName(String secondName);

    Boolean existsByEmail(String email);

    Boolean existsByFirstNameAndSecondName(String firstName, String secondName);

    Boolean existsByPosition_Id(Long id);

    Optional<User> findByFirstNameAndSecondNameAndPatronymic(String firstName, String secondName, String patronymic);

    Page<User> findAll(Specification<User> userSpecification, Pageable pageable);
}
