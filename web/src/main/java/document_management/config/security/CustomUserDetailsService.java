package document_management.config.security;

import document_management.entity.user.User;
import document_management.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    public static final String USER_NAMES_DELIMITER = ":";

    private UserRepository userRepository;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        String[] names = username.split(USER_NAMES_DELIMITER);
        if (names.length < 3) {
            throw new UsernameNotFoundException("User doesn't exists");
        }

        User user = userRepository.findByFirstNameAndSecondNameAndPatronymic(names[0], names[1], names[2])
                .orElseThrow(() ->
                        new UsernameNotFoundException("User doesn't exists"));

        return CustomUserDetails.fromUser(user);
    }
}
