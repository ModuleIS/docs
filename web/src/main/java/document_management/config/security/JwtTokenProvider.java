package document_management.config.security;

import document_management.protocol.dto.AuthenticationRequestDTO;
import document_management.protocol.dto.UserDto;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;
import java.util.StringJoiner;

@Component
public class JwtTokenProvider {

    public static final String CLAIM_FIRST_NAME = "firstName";
    public static final String CLAIM_SECOND_NAME = "secondName";
    public static final String CLAIM_ROLE = "role";

    private final UserDetailsService userDetailsService;

    @Value("${jwt.secret}")
    private String secretKey;
    @Value("${jwt.header}")
    private String authorizationHeader;
    @Value("${jwt.expiration}")
    private long validityInMilliseconds;

    public JwtTokenProvider(@Qualifier("customUserDetailsService") UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(UserDto userDto) {
        Claims claims = Jwts.claims().setSubject(userDto.getPatronymic());
        claims.put(CLAIM_FIRST_NAME, userDto.getFirstName());
        claims.put(CLAIM_SECOND_NAME, userDto.getSecondName());
        claims.put(CLAIM_ROLE, userDto.getRole().name());

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return !claimsJws.getBody().getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException("JWT token is expired or invalid", HttpStatus.UNAUTHORIZED);
        }
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(getFullNameFromToken(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    private String getFullNameFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        String surname = claims.getSubject();
        String fistName = (String) claims.get(CLAIM_FIRST_NAME);
        String secondName = (String) claims.get(CLAIM_SECOND_NAME);

        return getFullName(fistName, secondName, surname);
    }

    public String resolveToken(HttpServletRequest request) {
        return request.getHeader(authorizationHeader);
    }

    public Authentication toAuthenticate(AuthenticationRequestDTO request) {
        return new UsernamePasswordAuthenticationToken(
                getFullName(request.getFirstName(), request.getSecondName(),request.getPatronymic()),
                request.getPassword());
    }

    private  String getFullName(String firstName, String secondName, String surname){
        StringJoiner sj = new StringJoiner(CustomUserDetailsService.USER_NAMES_DELIMITER);
        sj.add(firstName).add(secondName).add(surname);
        return sj.toString();
    }
}