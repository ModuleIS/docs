package document_management.controller;

import document_management.config.security.JwtTokenProvider;
import document_management.entity.user.UserStatus;
import document_management.facadeService.api.UserFacadeService;
import document_management.protocol.dto.AuthenticationRequestDTO;
import document_management.protocol.dto.UserDto;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final UserFacadeService userFacadeService;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(@Valid @RequestBody AuthenticationRequestDTO request) {
        try {
            UserDto userDto = userFacadeService.findByFullName(request.getFirstName(), request.getSecondName(), request.getPatronymic());
            if (!UserStatus.valueOf(userDto.getStatus()).equals(UserStatus.DEACTIVATED)) {
                Authentication toAuthenticate = jwtTokenProvider.toAuthenticate(request);
                authenticationManager.authenticate(toAuthenticate);

                String token = jwtTokenProvider.createToken(userDto);

                Map<String, String> response = new HashMap<>();

                response.put("email", userDto.getEmail());
                response.put("secondName", request.getSecondName());
                response.put("token", token);
                userFacadeService.activation(userDto);
                userFacadeService.setOnline(userDto.getId());
                return ResponseEntity.ok(response);
            } else return new ResponseEntity<>("User DEACTIVATED.", HttpStatus.BAD_REQUEST);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>("Invalid login/password combination", HttpStatus.FORBIDDEN);
        }
    }


    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        String id = SecurityContextHolder.getContext().getAuthentication().getName();
        userFacadeService.setOffline(Long.parseLong(id));
        SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }
}