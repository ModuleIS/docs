package document_management.controller;


import document_management.facadeService.api.StorageFileFacadeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/file")
public class FileUploadController {

    private final StorageFileFacadeService storageService;

    @PostMapping("/upload")
    public ResponseEntity<?> handleFileUpload (@RequestParam("files") MultipartFile[] files) {

        List<String> savedFiles = Arrays.stream(files)
                .filter(f -> f!= null && !f.isEmpty())
                .map(storageService::uploadFile)
                .collect(Collectors.toList());

        if(savedFiles.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("File is NULL or empty");
        }
        return ResponseEntity.ok(savedFiles);
    }


    @GetMapping("/download/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename) throws IOException {
        Resource file = storageService.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(file.getFile().length())
                .body(file);
    }

}
