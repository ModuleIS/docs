package document_management.controller;

import document_management.facadeService.api.FolderSystemFacadeService;
import document_management.protocol.dto.FolderFullDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/folder")
@AllArgsConstructor
public class FolderSystemController {

    private final FolderSystemFacadeService folderSystemFacadeService;

    @GetMapping
    public ResponseEntity<List<FolderFullDTO>> getFolders() {
        return ResponseEntity.ok(folderSystemFacadeService.getAllFolders());
    }


}
