package document_management.controller;

import document_management.facadeService.api.UserFacadeService;
import document_management.protocol.request.ReminderPasswordRequest;
import document_management.protocol.request.ResetPasswordRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@PreAuthorize("hasAnyRole('ROLE_MAIN_ADMIN', 'ADMIN', 'USER')")
@RequestMapping("/password")
@RequiredArgsConstructor
public class PasswordController {

    private final UserFacadeService userFacadeService;

    @PreAuthorize("permitAll()")
    @PutMapping("/reset")
    public ResponseEntity<Boolean> resetPassword(@RequestBody ResetPasswordRequest request){
        return ResponseEntity.ok(userFacadeService.resetPassword(request));
    }

    @PostMapping("/reminder")
    public ResponseEntity<?> sendPassword(@RequestBody ReminderPasswordRequest request){
        userFacadeService.sendPassword(request);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/generate")
    public ResponseEntity<String> generatePassword(){
        return ResponseEntity.ok(userFacadeService.generatePassword().getPassword());
    }
}
