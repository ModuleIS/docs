package document_management.controller.exceptionHandler;

import document_management.exeption.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;


@Slf4j
@RestControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler({ResourceAlreadyExistsException.class, UserExistException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public String duplicateEntity(Exception e) {
        log.error("Error: " + e.getMessage(), e);
        return e.getMessage();
    }

    @ExceptionHandler({ResourceNotFoundException.class, UserNotFoundException.class,
            EmptyResultDataAccessException.class, EntityNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String notFoundEntity(Exception e) {
        log.error("Error: " + e.getMessage(), e);
        return e.getMessage();
    }

    @ExceptionHandler({ConstraintViolationException.class, ResourceDeleteException.class})
    @ResponseStatus(HttpStatus.LOCKED)
    public String lockedEntity(Exception e) {
        log.error("Error: " + e.getMessage(), e);
        return e.getMessage();
    }


    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String argumentErrorEntity(Exception e) {
        log.error("Error: " + e.getMessage(), e);
        return e.getMessage();
    }

}