package document_management.controller.superAdmin;

import document_management.facadeService.api.DocumentFacadeService;
import document_management.protocol.dto.DocumentCardDTO;
import document_management.protocol.request.CreateDocumentCardRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/document")
@AllArgsConstructor
public class DocumentController {

    private DocumentFacadeService documentFacadeService;


    @PostMapping("/create")
    public ResponseEntity<DocumentCardDTO> createDocument(@Valid @RequestBody CreateDocumentCardRequest request) {
        return ResponseEntity.ok(documentFacadeService.createDocument(request));
    }

    @GetMapping()
    public ResponseEntity<List<?>> getDocuments(
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "limit", defaultValue = "15", required = false) int limit ) {
        return ResponseEntity.ok(documentFacadeService.getDocuments(page, limit));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Long> deleteDocument(@PathVariable long id) {
        documentFacadeService.deleteDocument(id);
        return ResponseEntity.ok(id);
    }

    @GetMapping("{id}")
    public ResponseEntity<DocumentCardDTO> getDocument(@PathVariable long id) {
        return ResponseEntity.ok(documentFacadeService.getDocumentById(id));
    }

}
