package document_management.controller.superAdmin;

import document_management.facadeService.api.PermissionsFacadeService;
import document_management.protocol.dto.PermissionGroupDTO;
import document_management.protocol.dto.PermissionGroupFullDTO;
import document_management.protocol.request.PermissionGroupRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/permission")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ROLE_MAIN_ADMIN', 'ROLE_ADMIN')")
public class PermissionGroupController {

    private final PermissionsFacadeService permissionsFacadeService;

    @GetMapping
    public ResponseEntity<Page<PermissionGroupDTO>> getPermissionGroups(
            @RequestParam(value = "name", required = false) String filterByName,
            @PageableDefault(size = 10, page = 0)
            @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        Page<PermissionGroupDTO> result = permissionsFacadeService.getAllPermissionGroups(filterByName, pageable);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/create")
    public ResponseEntity<PermissionGroupDTO> createPermissionGroup(@Valid @RequestBody PermissionGroupRequest request) {
        return ResponseEntity.ok(permissionsFacadeService.createPermissionGroup(request));
    }

    @PutMapping("/update")
    public ResponseEntity<PermissionGroupDTO> changePermissionGroup(@Valid @RequestBody PermissionGroupRequest request){
        return ResponseEntity.ok(permissionsFacadeService.changePermissionGroup(request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deletePermissionGroup(@PathVariable long id){
        permissionsFacadeService.deletePermissionGroup(id);
        return ResponseEntity.ok(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PermissionGroupFullDTO> getPermissionGroup(@PathVariable long id){
        PermissionGroupFullDTO permissionGroupFullDTO = permissionsFacadeService.getPermissionGroupById(id);
        return ResponseEntity.ok(permissionGroupFullDTO);
    }
    @GetMapping("/all")
    public ResponseEntity<List<PermissionGroupDTO>> getPermissionGroupsAll(){
        List<PermissionGroupDTO> result = permissionsFacadeService.getAllPermissionGroups();
        return ResponseEntity.ok(result);
    }

}
