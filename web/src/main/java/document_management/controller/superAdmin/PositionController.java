package document_management.controller.superAdmin;

import document_management.facadeService.api.PositionFacadeService;
import document_management.protocol.dto.PositionDTO;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@PreAuthorize("hasAnyRole('ROLE_MAIN_ADMIN', 'ADMIN')")
@RequestMapping("/position")
@AllArgsConstructor
public class PositionController {

    private final PositionFacadeService positionFacadeService;

    @GetMapping
    public ResponseEntity<Page<PositionDTO>> getPositionPage(
            @RequestParam(value = "name", required = false) String filterByName,
            @PageableDefault(size = 10, page = 0)
            @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        Page<PositionDTO> result = positionFacadeService.getAllUserPositions(filterByName, pageable);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/all")
    public ResponseEntity<List<PositionDTO>> getAllPositions() {
        List<PositionDTO> result = positionFacadeService.getAllPositions();
        return ResponseEntity.ok(result);
    }

    @PostMapping("/create")
    public ResponseEntity<PositionDTO> createPosition(@Valid @RequestBody PositionDTO request) {
        return ResponseEntity.ok(positionFacadeService.createPositions(request));
    }

    @PutMapping("/update")
    public ResponseEntity<PositionDTO> changePosition(@Valid @RequestBody PositionDTO request){
        return ResponseEntity.ok(positionFacadeService.changePosition(request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deletePosition(@PathVariable long id){
        positionFacadeService.deletePosition(id);
        return ResponseEntity.ok(id);
    }
}
