package document_management.controller.superAdmin;

import document_management.facadeService.api.UserFacadeService;
import document_management.protocol.dto.UserDto;
import document_management.protocol.request.*;
import document_management.protocol.specification.GetAllUsersRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@PreAuthorize("hasAnyRole('ROLE_MAIN_ADMIN', 'ADMIN')")
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserFacadeService userFacadeService;

    @PostMapping("/create")
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody CreateUserRequest request) {
        return ResponseEntity.ok(userFacadeService.createUser(request));
    }

    @PreAuthorize("hasAnyRole('ROLE_MAIN_ADMIN')")
    @PutMapping("/role")
    public ResponseEntity<UserDto> changeRole(@Valid @RequestBody ChangeRoleRequest request){
        return ResponseEntity.ok(userFacadeService.changeRole(request));
    }

    @PostMapping("/get/filter")
    public ResponseEntity<Page<UserDto>> getUsersByFilter(@RequestBody GetAllUsersRequest request, Pageable pageable){
        return ResponseEntity.ok(userFacadeService.getUserByFilter(request.getSpecification(), pageable));
    }

    @PutMapping("/edit")
    public ResponseEntity<UserDto> updateUser(@RequestBody UpdateUserRequest request){
        return ResponseEntity.ok(userFacadeService.updateUser(request));
    }

    @PutMapping("/status")
    public ResponseEntity<UserDto> changeStatus(@RequestBody ChangeStatusRequest request){
        return ResponseEntity.ok(userFacadeService.changeUserStatus(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Long id){
        return ResponseEntity.ok(userFacadeService.findUserById(id));
    }

}
