package document_management.controller.user;

import document_management.facadeService.api.AssignmentFacadeService;
import document_management.protocol.dto.AssignmentDTO;
import document_management.protocol.dto.DocumentFieldDTO;
import document_management.protocol.dto.DocumentTypeDTO;
import document_management.protocol.dto.DocumentTypeFullDTO;
import document_management.protocol.request.AssignmentRequest;
import document_management.protocol.request.DocumentTypeRequest;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/assignment")
@AllArgsConstructor
public class AssignmentController {

    private final AssignmentFacadeService facadeService;

    @PostMapping("/create")
    public ResponseEntity<AssignmentDTO> createAssignment(@Valid @RequestBody AssignmentRequest request) {
        return ResponseEntity.ok(facadeService.createAssignment(request));
    }

    @GetMapping
    public ResponseEntity<Page<AssignmentDTO>> getAssignmentPage(
            @PageableDefault(size = 10, page = 0)
            @SortDefault(sort = "term", direction = Sort.Direction.ASC) Pageable pageable) {
        Page<AssignmentDTO> result = facadeService.getAllAssignments(pageable);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteAssignment(@PathVariable long id){
        facadeService.deleteAssignment(id);
        return ResponseEntity.ok(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AssignmentDTO> getAssignmentById(@PathVariable long id) {
        AssignmentDTO result = facadeService.getAssignmentById(id);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/update")
    public ResponseEntity<AssignmentDTO> changeAssignment(@Valid @RequestBody AssignmentRequest request){
        return ResponseEntity.ok(facadeService.updateAssignment(request));
    }
}
