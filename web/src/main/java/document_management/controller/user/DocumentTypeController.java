package document_management.controller.user;

import document_management.facadeService.api.DocumentTypeFacadeService;
import document_management.protocol.dto.DocumentFieldDTO;
import document_management.protocol.dto.DocumentTypeDTO;
import document_management.protocol.dto.DocumentTypeFullDTO;
import document_management.protocol.request.DocumentTypeRequest;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/document-type")
@AllArgsConstructor
public class DocumentTypeController {

    private final DocumentTypeFacadeService documentTypeFacadeService;

    @GetMapping("/actions")
    private List<String> getAvailableActions(){
        return documentTypeFacadeService.getAllDocumentActions();
    }

    @GetMapping("/field")
    public ResponseEntity<List<DocumentFieldDTO>> getAllFields() {
        List<DocumentFieldDTO> result = documentTypeFacadeService.getAllDocumentFields();
        return ResponseEntity.ok(result);
    }

    @GetMapping("/field/types")
    public ResponseEntity<List<String>> getAllFieldTypes() {
        List<String> result = documentTypeFacadeService.getAllDocumentFieldTypes();
        return ResponseEntity.ok(result);
    }

    @PostMapping("/field/create")
    public ResponseEntity<DocumentFieldDTO> createDocField(@Valid @RequestBody DocumentFieldDTO request) {
        return ResponseEntity.ok(documentTypeFacadeService.createDocumentField(request));
    }

    @PutMapping("/field/update")
    public ResponseEntity<DocumentFieldDTO> changeDocField(@Valid @RequestBody DocumentFieldDTO request){
        return ResponseEntity.ok(documentTypeFacadeService.changeDocumentField(request));
    }

    @DeleteMapping("/field/{id}")
    public ResponseEntity<Long> deleteDocField(@PathVariable long id){
        documentTypeFacadeService.deleteDocumentField(id);
        return ResponseEntity.ok(id);
    }

    @GetMapping
    public ResponseEntity<Page<DocumentTypeDTO>> getDocumentTypePage(
            @RequestParam(value = "name", required = false) String filterByName,
            @PageableDefault(size = 10, page = 0)
            @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        Page<DocumentTypeDTO> result = documentTypeFacadeService.getAllDocumentTypesPage(filterByName, pageable);
        return ResponseEntity.ok(result);
    }
    @GetMapping("/all")
    public ResponseEntity<List<DocumentTypeDTO>> getAllDocumentTypes() {
        List<DocumentTypeDTO> result = documentTypeFacadeService.getAllDocumentTypes();
        return ResponseEntity.ok(result);
    }

    @PostMapping("/create")
    public ResponseEntity<DocumentTypeFullDTO> createDocType(@Valid @RequestBody DocumentTypeRequest request) {
        return ResponseEntity.ok(documentTypeFacadeService.createDocumentType(request));
    }

    @PutMapping("/update")
    public ResponseEntity<DocumentTypeFullDTO> changeDocType(@Valid @RequestBody DocumentTypeRequest request){
        return ResponseEntity.ok(documentTypeFacadeService.changeDocumentType(request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteDocType(@PathVariable long id){
        documentTypeFacadeService.deleteDocumentType(id);
        return ResponseEntity.ok(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DocumentTypeFullDTO> getDocumentTypeById(@PathVariable long id) {
        DocumentTypeFullDTO result = documentTypeFacadeService.getAllDocumentTypeById(id);
        return ResponseEntity.ok(result);
    }

    @PostMapping("{id}/add-template")
    public ResponseEntity<DocumentTypeFullDTO> addTemplate(@PathVariable ("id") long id,
                                               @RequestParam("file") MultipartFile file) {
        DocumentTypeFullDTO result = documentTypeFacadeService.addTemplateForType(id, file);
        return ResponseEntity.ok(result);
    }


    @DeleteMapping("{id}/delete-template/{templateId}")
    public ResponseEntity<Long> deleteTemplate(@PathVariable ("id") long id,
                                               @PathVariable("templateId") long templateId) {
        documentTypeFacadeService.deleteTemplateForType(id, templateId);
        return ResponseEntity.ok(templateId);
    }


}
