INSERT INTO public.position (  id, name)  VALUES (1, 'Головний адміністратор');
INSERT INTO public.users(  id, creation_date, email, first_name, password, patronymic, phone, position_id, role, second_name, status)
   VALUES (1, now(), 'admin@email.com', 'admin', '$2a$10$tOyjtFVF./uX9lJ4cZFdkuLKKTX9Ykm4C2n0kZnR9iC627CtGuzQ2', 'admin', null, 1, 'MAIN_ADMIN', 'admin', 'ACTIVE');